//---------------------------------------------------------------------------

#include <ui/routines/animation.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        void animation::resume() noexcept {
            if (active() || _current_time >= _duration || !_callback) {
                return;
            }

            set_active(true);
            update();
        }

        void animation::pause() noexcept {
            set_active(false);
        }

        void animation::reset() noexcept {
            set_time(std::chrono::milliseconds::zero());
        }

        void animation::operator()(duration_type delta) noexcept {
            BOOST_ASSERT_MSG(active(), "Trying to run inactive animation");
            set_time(_current_time + delta);
        }

        void animation::set_time(const std::chrono::milliseconds & time) noexcept {
            if (time >= _duration) {
                _current_time = _duration;
                pause();
            } else {
                _current_time = time;
            }

            update();
        }

        void animation::update() noexcept {
            if (!_callback) {
                return;
            }

            _callback(static_cast<progress_type>(_current_time.count()) / _duration.count());
        }
    }
}

//---------------------------------------------------------------------------
