import os

from conans import ConanFile, CMake

project_name = "ui"


class UiConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "UI primitives"
    settings = "os", "compiler", "build_type", "arch"
    topics = ("asd", "ui")
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.container/0.0.1@asd/testing",
        "asd.space/0.0.1@asd/testing",
        "asd.gfx/0.0.1@asd/testing",
        "asd.geometry/0.0.1@asd/testing",
        "asd.typography/0.0.1@asd/testing",
        "asd.routine/0.0.1@asd/testing",
        "asd.signal/0.0.1@asd/testing",
        "asd.symbol/0.0.1@asd/testing",
		"asd.ecs/0.0.1@asd/testing",
        "pfr/2.0.2"
    )
    options = {"gfx": ["opengl"]}
    default_options = {"gfx": "opengl"}

    def requirements(self):
        if self.options.gfx == "opengl":
            self.requires("asd.opengl/0.0.1@asd/testing")

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.h", dst="generated/include", src="generated/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.includedirs = ["include", "generated/include"]
        self.cpp_info.libs = [project_name]
