//---------------------------------------------------------------------------

#define CATCH_CONFIG_RUNNER
#include <catch2/catch_all.hpp>

#include <launch/main.h>

#include <ui/systems/context.h>
#include <ui/systems/geometry_system.h>

#include <ui/models/nesting_model.h>
#include <ui/models/stacking_model.h>

#include <color/preset.h>
#include <color/fmt.h>

#include <math/fmt.h>
#include <boost/di.hpp>

//---------------------------------------------------------------------------

namespace asd::app
{
    template <class T>
    struct padding
    {
        void nest(entt::registry &, entt::entity, ui::geometry & geometry) const {
            geometry.area.resize_by(-width);
        }

        space::real width = 0.0f;
    };

    using outer_padding = padding<struct outer>;
    using border_padding = padding<struct border>;

    struct border_layer
    {
        color::linear_rgb color;
        space::real radius = 0.0f;
    };

    struct color_layer
    {
        color::linear_rgb color;
    };

    struct shadow_layer
    {
        color::linear_rgb color;
        space::point offset;
        space::real radius = 0.0f;
        space::real blur = 0.0f;
    };

    struct widget
    {
        struct def
        {
            static constexpr meta::type<ui::geometry> geometry{};
            static constexpr meta::type<border_layer> border{};
            static constexpr meta::type<shadow_layer> shadow{};
            static constexpr meta::type<color_layer> background{};
            static constexpr meta::type<outer_padding> outer_padding{};
            static constexpr meta::type<border_padding> border_padding{};
        };

        static constexpr auto model_def = ui::nest(
            def::geometry,
            def::outer_padding,
            ui::stack(
                def::shadow,
                ui::nest(
                    def::border,
                    def::border_padding,
                    def::background
                )
            )
        );

        template <class Context>
        widget(Context & context) noexcept :
            handle(ui::make_model(context, model_def))
        {}

        widget(widget &&) noexcept = default;

        widget & operator = (widget && w) noexcept = default;

        const ui::geometry & geometry() const noexcept {
            return handle[def::geometry];
        }

        void update(const ui::geometry & geometry) noexcept {
            handle.update(geometry);
        }

        ecs::handle handle;
    };

    template <class Renderer>
    struct stateful_renderer
    {
        static constexpr auto tags = ui::tags::stateful;

        template <class ModelDef>
        void populate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.emplace<typename Renderer::render_data>(entity);
        }

        template <class ModelDef>
        void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.emplace<typename Renderer::render_data>(entity);
        }
    };

    struct color_renderer : stateful_renderer<color_renderer>
    {
        static constexpr auto components = meta::type_v<color_layer>;
        static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

        struct render_data
        {
            color::linear_rgb color;
            space::rect area;
        };

        color_renderer(entt::registry & registry) :
            observer(registry, entt::collector.update<color_layer>().where<render_data>()) {}

        color_renderer(color_renderer &&) noexcept = default;

        ui::geometry reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
            auto & r = registry.get<render_data>(entity);
            r.area = geometry.area;

            return geometry;
        }

        void update(entt::registry & registry) {
            observer.each([&](const auto entity) {
                auto [m, r] = registry.get<color_layer, render_data>(entity);
                r.color = m.color;
            });
        }

        void render(entt::registry & registry) {
            data.clear();

            auto view = registry.view<render_data>();

            for (auto entity : view) {
                auto [r] = view.get(entity);
                data.push_back(r);
            }
        }

        std::vector<render_data> data;
        entt::observer observer;
    };

    struct border_renderer : stateful_renderer<border_renderer>
    {
        static constexpr auto components = meta::type_v<border_layer>;
        static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

        struct render_data
        {
            color::linear_rgb color;
            space::rect area;
        };

        border_renderer(entt::registry & registry) :
            observer(registry, entt::collector.update<border_layer>().where<render_data>()) {}

        border_renderer(border_renderer &&) noexcept = default;

        ui::geometry reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
            auto & r = registry.get<render_data>(entity);
            r.area = geometry.area;

            return geometry;
        }

        void update(entt::registry & registry) {
            observer.each([&](const auto entity) {
                auto [m, r] = registry.get<border_layer, render_data>(entity);
                r.color = m.color;
            });
        }

        void render(entt::registry & registry) {
            data.clear();

            auto view = registry.view<render_data>();

            for (auto entity : view) {
                auto [r] = view.get(entity);
                data.push_back(r);
            }
        }

        std::vector<render_data> data;
        entt::observer observer;
    };

    struct shadow_renderer : stateful_renderer<shadow_renderer>
    {
        static constexpr auto components = meta::type_v<shadow_layer>;
        static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

        struct render_data
        {
            color::linear_rgb color;
            space::rect area;
            space::vector params;
        };

        shadow_renderer(entt::registry & registry) :
            observer(registry, entt::collector.update<shadow_layer>().where<render_data>()) {}

        shadow_renderer(shadow_renderer &&) noexcept = default;

        ui::geometry reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
            auto [m, r] = registry.get<shadow_layer, render_data>(entity);

            auto shadow_area = geometry.area.resized_by(m.radius);
            auto blur = 0.5f * m.blur / shadow_area.size();
            auto radius = m.radius / shadow_area.size();

            r.area = shadow_area + m.offset;
            r.params = space::vector(blur.x, blur.y, radius.x, radius.y);

            return geometry;
        }

        void update(entt::registry & registry) {
            observer.each([&](const auto entity) {
                auto [m, r] = registry.get<shadow_layer, render_data>(entity);
                r.color = m.color;
            });
        }

        void render(entt::registry & registry) {
            data.clear();

            auto view = registry.view<render_data>();

            for (auto entity : view) {
                auto [r] = view.get(entity);
                data.push_back(r.color);
            }
        }

        std::vector<color::rgb> data;
        entt::observer observer;
    };

    constexpr meta::type<color_renderer> color_renderer_type{};
    constexpr meta::type<border_renderer> border_renderer_type{};
    constexpr meta::type<shadow_renderer> shadow_renderer_type{};

    constexpr auto system_types =
        ui::def::geometry_system +
        shadow_renderer_type +
        border_renderer_type +
        color_renderer_type;

    using context_type = ecs::context_of<system_types>;

    TEST_CASE("widget model", "[asd::ui]") {
        auto root = boost::di::make_injector();
        context_type & context = root.create<context_type &>();

        widget w(context);

        using def = widget::def;

        REQUIRE(w.handle.all_of(
            def::geometry,
            def::outer_padding,
            def::shadow,
            def::border,
            def::border_padding,
            def::background
        ));

        REQUIRE(w.handle.all_of(
            meta::type_v<color_renderer::render_data>,
            meta::type_v<border_renderer::render_data>,
            meta::type_v<shadow_renderer::render_data>
        ));

        w.handle.patch(def::background + def::border + def::border_padding + def::outer_padding + def::shadow, [&](auto & background, auto & border, auto & border_padding, auto & padding, auto & shadow) {
            background.color = color::white;
            border.color = color::gray;
            border_padding.width = 4.0f;
            padding.width = 4.0f;
            shadow.color = color::dark_gray;
        });

        auto & background_renderer = context(color_renderer_type);
        auto & border_renderer = context(border_renderer_type);
        auto & shadow_renderer = context(shadow_renderer_type);

        w.update(ui::geometry{{0.0f, 0.0f, 100.0f, 100.0f}, 0.0f});

        ecs::update(context);
        ui::render(context);

        // check nesting and render data updates
        REQUIRE(shadow_renderer.data.size() == 1);
        REQUIRE(shadow_renderer.data[0] == color::dark_gray);

        REQUIRE(border_renderer.data.size() == 1);
        REQUIRE(border_renderer.data[0].color == color::gray);
        REQUIRE(border_renderer.data[0].area == space::rect{4.0f, 4.0f, 96.0f, 96.0f});

        REQUIRE(background_renderer.data.size() == 1);
        REQUIRE(background_renderer.data[0].color == color::white);
        REQUIRE(background_renderer.data[0].area == space::rect{8.0f, 8.0f, 92.0f, 92.0f});

        w.handle.patch(def::background, [&](auto & b) { b.color = color::gold; });

        ecs::update(context);
        ui::render(context);

        REQUIRE(background_renderer.data[0].color == color::gold); // render data was updated
        REQUIRE(background_renderer.data[0].area == space::rect{8.0f, 8.0f, 92.0f, 92.0f}); // but rendered area remains the same

        w.handle.patch(def::background, [&](auto & b) { b.color = color::green; });

        REQUIRE(background_renderer.data[0].color == color::gold); // render data was not updated yet

        ecs::update(context);
        ui::render(context);

        REQUIRE(background_renderer.data[0].color == color::green);
    }

    static ::asd::entrance open([](int argc, char * argv[]) {
        Catch::Session session;

        if (int return_code = session.applyCommandLine(argc, argv); return_code != 0) {
            return return_code;
        }

        return session.run();
    });
}

//---------------------------------------------------------------------------
