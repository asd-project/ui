/**
 *
 */
#version 330 core

uniform sampler2D texture0;

in Vertex
{
    vec2 texcoord;
    vec4 color;
} vs;

out vec4 fscolor;

void main(void) {
    fscolor = vec4(vs.color.rgb, texture(texture0, vs.texcoord).r * vs.color.a);
}
