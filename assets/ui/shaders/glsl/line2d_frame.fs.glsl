/**
 *
 */
#version 330 core

layout(std140) uniform Color
{
    vec4 color;
};

in vec3 vs_texcoord;

out vec4 fscolor;

void main(void) {
    fscolor = vec4(color.rgb * color.a, color.a);
}
