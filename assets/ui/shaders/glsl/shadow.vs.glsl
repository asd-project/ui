/**
 *    !vertex: p3 t2 c4 g s
 */
#version 330 core

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec2 texcoord;
in vec4 color;
in vec4 geometry;
in vec4 shadow;

out Vertex 
{
    vec4 color;
    vec4 shadow;
    vec2 texcoord;
} vs;

void main(void) {
    vec3 p = vec3(geometry.x + geometry.z, geometry.y + geometry.w, 0.0) + vec3(geometry.z - geometry.x, geometry.y - geometry.w, 2.0) * position;
    gl_Position = projection * view * vec4(0.5 * p, 1.0);
    vs.color = color;
    vs.shadow = shadow;
    vs.texcoord = texcoord;
}
