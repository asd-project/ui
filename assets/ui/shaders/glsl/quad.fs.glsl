/**
 *
 */
#version 330 core

in Vertex
{
    vec4 color;
} vs;

out vec4 fscolor;

void main(void) {
    fscolor = vs.color;
}
