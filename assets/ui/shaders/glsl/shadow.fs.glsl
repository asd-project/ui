/**
 *
 */
#version 330 core

in Vertex
{
    vec4 color;
    vec4 shadow; // xy = blur, zw = unused
    vec2 texcoord;
} vs;

out vec4 fs_color;

vec4 erf(vec4 x)
{
    vec4 s = sign(x), a = abs(x);
    x = 1.0 + (0.278393 + (0.230389 + 0.078108 * a * a) * a) * a;
    x *= x;
    return s - s / (x * x);
}

float shadow(vec2 point)
{
      vec4 query = vec4(point - vs.shadow.xy, point + vs.shadow.xy - vec2(1.0));
      vec2 scale = sqrt(2) / vs.shadow.xy;
      vec4 integral = 0.5 + 0.5 * erf(query * vec4(scale, scale));
      return (integral.z - integral.x) * (integral.w - integral.y);
}

void main(void)
{
    fs_color = vec4(vs.color.rgb, vs.color.a * shadow(vs.texcoord));
}
