/**
 *  !vertex: p3 t3
 */
#version 330 core

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec3 texcoord;

void main() {
    gl_Position = projection * view * vec4(position, 1.0);
}