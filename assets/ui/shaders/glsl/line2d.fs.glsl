/**
 *
 */
#version 330 core

layout(std140) uniform Color
{
    vec4 color;
};

in vec3 vs_texcoord;

out vec4 fscolor;

vec3 rotate_hue(vec3 color, float hue) {
    const vec3 k = vec3(0.57735, 0.57735, 0.57735);
    float cosAngle = cos(hue);
    return vec3(color * cosAngle + cross(k, color) * sin(hue) + k * dot(k, color) * (1.0 - cosAngle));
}

void main(void) {
    // fscolor = color;
    float t = vs_texcoord.z * 0.5;
    float d = (t + 1.0) * abs(vs_texcoord.y) - t;
    float a = clamp(color.a * (1.0 - d * d * d), 0.0, 1.0);
    fscolor = vec4(color.rgb * a, a);
}
