/**
 *    !vertex: p3 c4 g
 */
#version 330 core

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec4 color;
in vec4 geometry;

out Vertex 
{
    vec4 color;
} vs;

vec3 apply_geometry(vec3 position) {
    return 0.5 * (vec3(geometry.x + geometry.z, geometry.y + geometry.w, 0.0) + vec3(geometry.z - geometry.x, geometry.y - geometry.w, 2.0) * position);
}

void main(void) {
    gl_Position = projection * view * vec4(apply_geometry(position), 1.0);
    vs.color = color;
}
