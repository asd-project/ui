/**
 *  !vertex: p2 t3
 */
#version 330 core

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec2 position;
in vec3 texcoord;

out vec3 vs_texcoord;

void main() {
    vs_texcoord = texcoord;
    gl_Position = projection * view * vec4(position, -0.1, 1.0);
}
