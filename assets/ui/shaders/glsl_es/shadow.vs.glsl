#version 300 es

/**
 *    !vertex: p3 t2 c4 g s
 */

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec2 texcoord;
in vec4 color;
in vec4 geometry;
in vec4 shadow;

out vec4 vs_color;
out vec4 vs_shadow;
out vec2 vs_texcoord;

void main(void) {
    vec3 p = vec3(geometry.x + geometry.z, geometry.y + geometry.w, 0.0) + vec3(geometry.z - geometry.x, geometry.y - geometry.w, 2.0) * position;
    gl_Position = projection * view * vec4(0.5 * p, 1.0);
    vs_color = color;
    vs_shadow = shadow;
    vs_texcoord = texcoord;
}
