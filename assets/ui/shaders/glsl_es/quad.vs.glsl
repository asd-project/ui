#version 300 es

/**
 *    !vertex: p3 c4 g
 */

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec4 color;
in vec4 geometry;

out vec4 vs_color;

void main(void) {
    vec3 p = vec3(geometry.x + geometry.z, geometry.y + geometry.w, 0.0) + vec3(geometry.z - geometry.x, geometry.y - geometry.w, 2.0) * position;
    gl_Position = projection * view * vec4(0.5 * p, 1.0);
    vs_color = color;
}
