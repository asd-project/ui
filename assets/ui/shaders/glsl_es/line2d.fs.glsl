#version 300 es

precision highp float;

layout(std140) uniform Color
{
    vec4 color;
};

in vec3 vs_texcoord;

out vec4 fscolor;

void main(void) {
    float t = vs_texcoord.z * 0.25;
    float d = (t + 1.0) * abs(vs_texcoord.y) - t;
    fscolor = vec4(color.rgb, color.a * min(1.0, 1.0 - d * d * d));
}
