#version 300 es

precision highp float;

in vec4 vs_color;

out vec4 fscolor;

void main(void) {
    fscolor = vs_color;
}
