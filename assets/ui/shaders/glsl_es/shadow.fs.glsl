#version 300 es

precision highp float;

in vec4 vs_color;
in vec4 vs_shadow; // xy = blur, zw = unused
in vec2 vs_texcoord;

out vec4 fs_color;

vec4 erf(vec4 x)
{
    vec4 s = sign(x), a = abs(x);
    x = 1.0 + (0.278393 + (0.230389 + 0.078108 * a * a) * a) * a;
    x *= x;
    return s - s / (x * x);
}

float shadow(vec2 point)
{
      vec4 query = vec4(point - vs_shadow.xy, point + vs_shadow.xy - vec2(1.0));
      vec2 scale = sqrt(2.0) / vs_shadow.xy;
      vec4 integral = 0.5 + 0.5 * erf(query * vec4(scale, scale));
      return (integral.z - integral.x) * (integral.w - integral.y);
}

void main(void)
{
    fs_color = vec4(vs_color.rgb, vs_color.a * shadow(vs_texcoord));
}
