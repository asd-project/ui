#version 300 es

precision highp float;

/**
 *    !vertex: p2 t2 c4
 */

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

layout(std140) uniform Atlas
{
    vec4 atlas_size;
};

in vec2 position;
in vec2 texcoord;
in vec4 color;

out Vertex 
{
    vec2 texcoord;
    vec4 color;
} vs;

void main(void) {
    gl_Position = projection * view * vec4(position, 0.0, 1.0);
    vs.texcoord = texcoord / atlas_size.xy;
    vs.color = color;
}
