//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/layouts/layout.h>
#include <ui/widgets/div.h>

#include <meta/tuple/unpack.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        template <class T>
        constexpr bool has_width_v = requires {
            { T::width };
        };

        template <class T>
        constexpr bool has_height_v = requires {
            { T::height };
        };

        namespace widgets
        {
            template <ui::layout Layout, ui::widget Element>
            class container : public ui::widgets::basic_widget
            {
            public:
                container(Layout && layout) noexcept :
                    _layout(std::move(layout)) {}

                container(container && c) noexcept = default;
                container & operator = (container && c) noexcept = default;

                size_t size() const noexcept {
                    return _elements.size();
                }

//---------------------------------------------------------------------------

                template <class T>
                auto & push_back(T && w) {
                    return _elements.emplace_back(validate_element(std::forward<T>(w)));
                }

                template <class T>
                auto & insert(size_t idx, T && w) {
                    return *_elements.insert(_elements.begin() + idx, validate_element(std::forward<T>(w)));
                }

                template <class T>
                auto & replace(size_t idx, T && w) noexcept {
                    return _elements[idx] = validate_element(std::forward<T>(w));
                }

                template <class T>
                auto & operator << (T && w) {
                    return push_back(std::forward<T>(w));
                }

//---------------------------------------------------------------------------

                template <class... A>
                auto & emplace_back(A &&... args) {
                    return _elements.emplace_back(std::forward<A>(args)...);
                }

                template <class... A>
                auto & emplace(size_t idx, A &&... args) {
                    return *_elements.emplace(_elements.begin() + idx, std::forward<A>(args)...);
                }

                void swap(size_t a, size_t b) noexcept {
                    auto t = std::move(_elements[a]);
                    _elements[a] = std::move(_elements[b]);
                    _elements[b] = std::move(t);
                }

                void erase(size_t idx) {
                    _elements.erase(_elements.begin() + idx);
                }

                void erase_widget(Element & w) {
                    _elements.erase(std::remove_if(_elements.begin(), _elements.end(), [&](auto & e) {
                        return &e == &w;
                    }));
                }

//---------------------------------------------------------------------------

                void update(const ui::geometry & geometry) noexcept {
                    widgets::basic_widget::update(_layout.update(_elements, geometry));
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept requires ui::hittable<Element> {
                    auto e = _layout.hit(_elements, pos);

                    if (e) {
                        e->hit(trace, pos);
                    }
                }

            protected:
                Element && validate_element(Element && w) {
                    return std::move(w);
                }

                template <class HorizontalSpan, class VerticalSpan>
                Element && validate_element(span_wrapper<Element, HorizontalSpan, VerticalSpan> && w) {
                    if constexpr (ui::has_width_v<Layout>) {
                        static_assert(HorizontalSpan::end < Layout::width, "Incorrect horizontal span for the widget: max value is out of bounds");
                    }

                    if constexpr (ui::has_height_v<Layout>) {
                        static_assert(VerticalSpan::end < Layout::height, "Incorrect vertical span for the widget: max value is out of bounds");
                    }

                    return std::move(w);
                }

                Layout _layout;
                std::vector<Element> _elements;
            };

            template <ui::layout Layout, ui::widget Element, size_t ElementsCount>
            class static_container
            {
            public:
                constexpr static_container(Layout && layout, std::array<Element, ElementsCount> && elements) noexcept :
                    _layout(std::move(layout)), _elements(std::move(elements)) {}

                constexpr static_container(static_container && c) noexcept = default;

                constexpr static_container & operator = (static_container && c) noexcept = default;

                constexpr size_t size() const noexcept {
                    return _elements.size();
                }

                constexpr auto & replace(size_t idx, Element && w) noexcept {
                    return _elements[idx] = std::move(w);
                }

                constexpr void swap(size_t a, size_t b) noexcept {
                    auto t = std::move(_elements[a]);
                    _elements[a] = std::move(_elements[b]);
                    _elements[b] = std::move(t);
                }

                constexpr const ui::geometry & geometry() const noexcept {
                    return _geometry;
                }

                constexpr void update(const ui::geometry & geometry) noexcept {
                    _geometry = _layout.update(_elements, geometry);
                }

                constexpr void hit(ui::trace & trace, const space::point & pos) noexcept requires ui::hittable<Element> {
                    auto e = _layout.hit(_elements, pos);

                    if (e) {
                        e->hit(trace, pos);
                    }
                }

            protected:
                ui::geometry _geometry;
                Layout _layout;
                std::array<Element, ElementsCount> _elements;
            };
        }

        struct container
        {
            template <class ... T> requires (
                symbol::matches<T...> (
                    s::layout   [symbol::required][symbol::unique]
                ) &&
                ui::layout<symbol::named_type<s::type::layout, T...>>
            )
            static auto make_static(T &&... options) noexcept {
                using layout_type = symbol::named_type<s::type::layout, T...>;

                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);
                constexpr auto count = symbol::unnamed_count<T...>;

                static_assert(count > 0, "Container should not be empty");

                using element_type = ui::unwrapped<symbol::unnamed_type<0, T...>>;
                using result_type = widgets::static_container<layout_type, element_type, count>;

                auto && layout = named[s::layout];

                return meta::unpack(unnamed, [&layout](auto && ... elements) {
                    return result_type {std::move(layout), std::array<element_type, count>{std::forward<element_type>(elements)...}};
                });
            }

            template <class... T> requires (
                symbol::matches<T...> (
                    s::layout       [symbol::required][symbol::unique],
                    s::element_type [symbol::required][symbol::unique]
                ) &&
                ui::layout<symbol::named_type<s::type::layout, T...>> &&
                ui::widget<symbol::named_type<s::type::element_type, T...>>
            )
            static auto make_dynamic(T &&... options) noexcept {
                using layout_type = symbol::named_type<s::type::layout, T...>;
                using element_type = symbol::named_type<s::type::element_type, T...>;

                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                using result_type = widgets::container<layout_type, element_type>;
                constexpr auto unnamed_count = symbol::unnamed_count<T...>;

                static_assert(
                    unnamed_count == 0 || (unnamed_count == 1 && std::is_invocable_v<symbol::unnamed_type<0, T...>, result_type &>),
                    "ui::container can take only one positional argument, which is callable with signature void(ui::widgets::container<...> &)"
                );

                if constexpr (unnamed_count == 1) {
                    result_type container {s::layout(named)};

                    auto && init = unnamed[0_c];
                    init(container);

                    return container;
                } else {
                    return result_type {s::layout(named)};
                }
            }
        };
    }
}
