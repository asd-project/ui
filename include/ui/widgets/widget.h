//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/concepts.h>

#include <ui/controllers/controller.h>
#include <ui/components/geometry.h>
#include <ui/model.h>

#include <entt/poly/poly.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace widgets
        {
            class basic_widget
            {
            public:
                basic_widget() noexcept = default;

                basic_widget(basic_widget &&) noexcept = default;

                basic_widget & operator = (basic_widget && w) noexcept = default;

                const ui::geometry & geometry() const noexcept {
                    return _geometry;
                }

                void update(const ui::geometry & geometry) noexcept {
                    _geometry = geometry;
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept {
                    //
                }

            protected:
                ui::geometry _geometry;
            };

            class model_widget
            {
            public:
                struct def
                {
                    static constexpr auto geometry = meta::type_v<ui::geometry>;
                };

                model_widget(ecs::handle && model) noexcept :
                    _model(std::move(model))
                {}

                model_widget(model_widget &&) noexcept = default;

                model_widget & operator = (model_widget && w) noexcept = default;

                ecs::ref & model_ref() noexcept {
                    return _model;
                }

                const ui::geometry & geometry() const noexcept {
                    return _model[def::geometry];
                }

                void update(const ui::geometry & geometry) noexcept {
                    _model.patch(def::geometry, [&](auto & g) {
                        if (g == geometry) {
                            return false;
                        }

                        g = geometry;
                        return true;
                    });
                }

            protected:
                ecs::handle _model;
            };

            struct any_widget_traits : entt::type_list<
                const ui::geometry & () const,
                void (const ui::geometry &),
                void (ui::trace &, const space::point &)
            >
            {
                template <class T>
                struct type : T
                {
                    const ui::geometry & geometry() const noexcept {
                        return entt::poly_call<0>(*this);
                    }

                    void update(const ui::geometry & geometry) noexcept {
                        entt::poly_call<1>(*this, geometry);
                    }

                    void hit(ui::trace & trace, const space::point & pos) noexcept {
                        entt::poly_call<2>(*this, trace, pos);
                    }
                };

                template<class T>
                static const ui::geometry & geometry(const T & widget) noexcept {
                    return widget.geometry();
                }

                template<class T>
                static void update(T & widget, const ui::geometry & geometry) noexcept {
                    widget.update(geometry);
                }

                template<class T>
                static void hit(T & widget, ui::trace & trace, const space::point & pos) noexcept {
                    if constexpr (ui::hittable<T>) {
                        widget.hit(trace, pos);
                    }
                }

                template <class T>
                using impl = entt::value_list<
                    &geometry<T>,
                    &update<T>,
                    &hit<T>
                >;
            };

            using any_widget = entt::basic_poly<any_widget_traits, 0>;
        }

        template <class T>
        concept widget =
            std::is_nothrow_move_constructible_v<T> &&
            std::is_nothrow_move_assignable_v<T> &&
            requires (T v, const ui::geometry & geometry, ui::trace & trace, const space::point & pos) {
                { v.geometry() } -> convertible_to<ui::geometry>;
                { v.update(geometry) };
            };

        template <class T>
        concept wrapper = requires {
            typename T::wrapped_type;
        };

        template <class T>
        struct unwrapped_type : identity<T> {};

        template <ui::wrapper T>
        struct unwrapped_type<T> : identity<typename T::wrapped_type> {};

        template <class T>
        using unwrapped = typename unwrapped_type<T>::type;
    }
}
