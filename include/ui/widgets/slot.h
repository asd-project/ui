//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>
#include <ui/traits/wrapper.h>

#include <ui/constraints/horizontal_cell.h>
#include <ui/constraints/vertical_cell.h>
#include <ui/constraints/cell.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace widgets
        {
            template <ui::widget Content, class Constraint = ui::constraint>
            class slot :
                public widgets::basic_widget,
                public traits::wrapper<Content, Constraint>
            {
            public:
                using traits::wrapper<Content, Constraint>::wrapper;

                slot(slot &&) noexcept = default;
                slot & operator = (slot &&) noexcept = default;

                void update(const ui::geometry & geometry) noexcept {
                    this->_content.update(this->_constraint.apply(geometry));
                    widgets::basic_widget::update(this->_constraint.apply(this->_content.geometry()));
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept requires ui::hittable<Content> {
                    this->_content.hit(trace, pos);
                }
            };
        }

        static constexpr struct
        {
            template <class... T> requires (
                symbol::matches<T...> (
                    s::constraint   [symbol::optional][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                constexpr auto children_count = symbol::unnamed_count<T...>;
                static_assert(children_count == 1, "Slot accepts exactly one child which is its contents");

                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                auto && constraint = named.at(s::constraint, []() { return ui::constraint{}; });

                using content_type = symbol::unnamed_type<0, T...>;
                using constraint_type = plaintype(constraint);
                using widget_type = widgets::slot<content_type, constraint_type>;

                return widget_type(std::move(unnamed[0_c]), std::move(constraint));
            }
        } slot;

        static constexpr struct
        {
            template <class... T> requires (
                symbol::matches<T...> (
                    s::min   [symbol::optional][symbol::unique],
                    s::max   [symbol::optional][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                constexpr auto children_count = symbol::unnamed_count<T...>;
                static_assert(children_count == 1, "Slot accepts exactly one child which is its contents");

                using content_type = symbol::unnamed_type<0, T...>;

                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                auto && min = named.at(s::min, []() { return 1; });
                auto && max = named.at(s::max, []() { return std::numeric_limits<space::real>::max(); });

                return widgets::slot<content_type, vertical_cell>(
                    std::move(unnamed[0_c]),
                    vertical_cell(space::range(min, max))
                );
            }
        } vslot;

        static constexpr struct
        {
            template <class... T> requires (
                symbol::matches<T...> (
                    s::min   [symbol::optional][symbol::unique],
                    s::max   [symbol::optional][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                constexpr auto children_count = symbol::unnamed_count<T...>;
                static_assert(children_count == 1, "Slot accepts exactly one child which is its contents");

                using content_type = symbol::unnamed_type<0, T...>;

                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                auto && min = named.at(s::min, []() { return 1; });
                auto && max = named.at(s::max, []() { return std::numeric_limits<space::real>::max(); });

                return widgets::slot<content_type, horizontal_cell>(
                    std::move(unnamed[0_c]),
                    horizontal_cell(space::range(min, max))
                );
            }
        } hslot;

        static constexpr struct
        {
            template <class... T> requires (
                symbol::matches<T...> (
                    s::min   [symbol::optional][symbol::unique],
                    s::max   [symbol::optional][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                constexpr auto children_count = symbol::unnamed_count<T...>;
                static_assert(children_count == 1, "Slot accepts exactly one child which is its contents");

                using content_type = symbol::unnamed_type<0, T...>;

                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                auto && min = named.at(s::min, []() { return space::size(1); });
                auto && max = named.at(s::max, []() { return space::size(std::numeric_limits<space::real>::max()); });

                return widgets::slot<content_type, cell>(
                    std::move(unnamed[0_c]),
                    cell(space::size_range(min, max))
                );
            }
        } cell_slot;
    }
}
