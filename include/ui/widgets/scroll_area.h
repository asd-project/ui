//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>
#include <ui/traits/wrapper.h>
#include <ui/traits/interactive.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace widgets
        {
            template <ui::widget Content, class Constraint = ui::constraint>
            class scroll_area :
                public traits::wrapper<Content, Constraint>,
                public traits::interactive<scroll_area<Content, Constraint>>
            {
            public:
                using traits::wrapper<Content, Constraint>::wrapper;

                scroll_area(scroll_area &&) noexcept = default;
                scroll_area & operator = (scroll_area &&) noexcept = default;

                space::point offset() const noexcept {
                    return _offset;
                }

                bool set_offset(const space::point & offset) noexcept {
                    auto & content_rect = this->_content.geometry();
                    auto o = math::clamp(offset, math::min(math::point(this->_geometry.area.size() - content_rect.area.size()), {0.0f, 0.0f}), {0.0f, 0.0f});

                    if (o == _offset) {
                        return false;
                    }

                    _offset = o;
                    _changed = true;

                    return true;
                }

                const ui::geometry & geometry() const noexcept {
                    return _geometry;
                }

                void update(const ui::geometry & geometry) noexcept {
                    if (!_changed && _geometry == geometry) {
                        return;
                    }

                    _changed = false;
                    _geometry = geometry;

                    this->_content.update(this->_constraint.apply(this->_geometry) + _offset);

                    auto & content_rect = this->_content.geometry();
                    _offset = math::clamp(_offset, math::min(math::point(this->_geometry.area.size() - content_rect.area.size()), {0.0f, 0.0f}), {0.0f, 0.0f});
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept {
                    traits::interactive<scroll_area<Content, Constraint>>::hit(trace, pos);

                    if constexpr (ui::hittable<Content>) {
                        this->_content.hit(trace, pos);
                    }
                }

            private:
                ui::geometry _geometry;
                space::rect _content_rect;
                space::point _offset;
                bool _changed = false;
            };
        }

        template <class... T> requires (
            symbol::matches<T...> (
                s::controller   [symbol::optional][symbol::unique],
                s::constraint   [symbol::optional][symbol::unique]
            )
        )
        auto scroll_area(T &&... options) noexcept {
            constexpr auto children_count = symbol::unnamed_count<T...>;
            static_assert(children_count == 1, "Scroll area accepts exactly one child which is its contents");

            if constexpr(children_count == 1) {
                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                auto && constraint = named.at(s::constraint, []() { return ui::constraint{}; });

                using content_type = symbol::unnamed_type<0, T...>;
                using constraint_type = plaintype(constraint);
                using widget_type = widgets::scroll_area<content_type, constraint_type>;

                widget_type area(std::move(unnamed[0_c]), std::move(constraint));

                if constexpr (symbol::in<T...>(s::controller)) {
                    constexpr bool v = std::is_invocable_v<symbol::named_type<s::type::controller, T...>, widget_type &>;

                    if constexpr (v) {
                        area.set_controller(named[s::controller](area));
                    } else {
                        area.set_controller(std::move(named[s::controller]));
                    }
                }

                return area;
            }
        }
    }
}
