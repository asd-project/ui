//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>
#include <ui/traits/wrapper.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace widgets
        {
            class box : public traits::wrapper<any_widget>
            {
            public:
                using base_type = traits::wrapper<any_widget>;

                template <class T>
                    requires(!std::is_same_v<plain<T>, box> && ui::widget<T>)
                box(T && content) noexcept : base_type(std::forward<T>(content)) {}

                box(box &&) noexcept = default;
                box & operator = (box &&) noexcept = default;

                template <ui::widget T>
                const T * get_content() const noexcept {
                    if (this->_content.type().hash() != entt::type_hash<T>::value()) {
                        return nullptr;
                    }

                    return get_content_unsafe<T>();
                }

                template <ui::widget T>
                T * get_content() noexcept {
                    if (this->_content.type().hash() != entt::type_hash<T>::value()) {
                        return nullptr;
                    }

                    return get_content_unsafe<T>();
                }

                template <ui::widget T>
                void set_content(T && content) noexcept {
                    this->_content = any_widget(std::forward<T>(content));
                }

                decltype(auto) geometry() const noexcept {
                    return this->_content->geometry();
                }

                void update(const ui::geometry & geometry) noexcept {
                    this->_content->update(geometry);
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept {
                    this->_content->hit(trace, pos);
                }

                template <ui::widget T, class F>
                static auto configure(T && content, F && callback) {
                    widgets::box box{std::forward<T>(content)};
                    std::forward<F>(callback)(*box.get_content_unsafe<plain<T>>());

                    return box;
                }

            private:
                template <ui::widget T>
                const T * get_content_unsafe() const noexcept {
                    return static_cast<T *>(this->_content.data());
                }

                template <ui::widget T>
                T * get_content_unsafe() noexcept {
                    return static_cast<T *>(this->_content.data());
                }
            };
        }

        template <ui::widget T>
        auto box(T && content) {
            return ui::widgets::box{std::forward<T>(content)};
        }

        template <ui::widget T, class F>
        auto box(T && content, F && callback) {
            return ui::widgets::box::configure(std::forward<T>(content), std::forward<F>(callback));
        }
    }
}
