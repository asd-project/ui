//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace widgets
        {
            template <ui::widget T>
            struct div
            {
                div(math::rect<int> && span, T && widget) noexcept :
                    span(std::move(span)),
                    widget(std::move(widget))
                {}

                div(div &&) noexcept = default;
                div & operator = (div &&) noexcept = default;

                const ui::geometry & geometry() const noexcept {
                    return widget.geometry();
                }

                void update(const ui::geometry & geometry) noexcept {
                    widget.update(geometry);
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept requires ui::hittable<T> {
                    widget.hit(trace, pos);
                }

                math::rect<int> span;
                T widget;
            };

            template <int Min, int Max = Min>
            struct span {
                static_assert(Min >= 0, "span<Min, Max>: Incorrect range, Min should be non-negative integer");
                static_assert(Min <= Max, "span<Min, Max>: Incorrect range, Min should be <= Max");

                static constexpr int min = Min;
                static constexpr int max = Max;
            };
        }

        template <class T, class HorizontalSpan, class VerticalSpan>
        struct span_wrapper : T
        {
            using wrapped_type = T;

            using T::T;
            using T::operator =;
        };

        template <int Min, int Max = Min>
        static constexpr widgets::span<Min, Max> span {};

        template <class Horizontal, class Vertical, class Widget>
        auto div(Horizontal && horizontal, Vertical && vertical, Widget && widget) noexcept {
            using horizontal_type = plain<Horizontal>;
            using vertical_type = plain<Vertical>;
            using widget_type = plain<Widget>;
            using wrapper_type = span_wrapper<widgets::div<widget_type>, horizontal_type, vertical_type>;

            return wrapper_type{
                {horizontal_type::min, vertical_type::min, horizontal_type::max, vertical_type::max},
                std::forward<Widget>(widget)
            };
        }
    }
}
