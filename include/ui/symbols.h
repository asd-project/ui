//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <symbol/symbol.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        using namespace meta::literals;

        namespace s
        {
            define_symbol(color);
            define_symbol(constraint);
            define_symbol(content);
            define_symbol(controller);
            define_symbol(deceleration);
            define_symbol(direction);
            define_symbol(element_type);
            define_symbol(font);
            define_symbol(generate);
            define_symbol(horizontal);
            define_symbol(hub);
            define_symbol(init);
            define_symbol(input_period);
            define_symbol(layout);
            define_symbol(margin);
            define_symbol(max);
            define_symbol(min);
            define_symbol(on_click);
            define_symbol(on_press);
            define_symbol(on_release);
            define_symbol(on_hovered_change);
            define_symbol(on_pressed_change);
            define_symbol(on_scroll);
            define_symbol(context);
            define_symbol(routine_context);
            define_symbol(scheme);
            define_symbol(spacing);
            define_symbol(text);
            define_symbol(vertical);
            define_symbol(wheel_scroll_amount);
        }
    }
}
