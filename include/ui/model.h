//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ecs/context.h>

#include <meta/tuple/accumulate.h>

#include <ui/components/geometry.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class Model>
    concept container_model = requires(ecs::context<meta::empty_t> & context, entt::entity entity, const ui::geometry & geometry) {
        typename Model::contents_type;
        { Model::reshape(context, entity, geometry) };
    };

    template<class ModelDef>
    constexpr auto collect_components(ModelDef model_def) {
        using model_type = typename plaintype(model_def)::type;

        if constexpr (container_model<model_type>) {
            using contents_type = typename model_type::contents_type;

            constexpr auto current_type = [] {
                if constexpr (std::is_empty_v<model_type>) { // check if model has actual data
                    return meta::empty;
                } else {
                    return meta::type_v<model_type>;
                }
            } ();

            return meta::accumulate(contents_type{}, current_type, [](auto acc, auto submodel) {
                return acc + collect_components(submodel);
            });
        } else {
            return model_def;
        }
    }
}
