//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/model.h>
#include <ui/systems/geometry_system.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class Model>
    concept nestable = requires (Model m, ecs::context<meta::empty_t> & context, entt::entity entity, ui::geometry & geometry) {
        { m.nest(context, entity, geometry) };
    };

    template <class ... T>
    struct nesting_model
    {
        using contents_type = meta::types<T...>;
        static constexpr contents_type contents{};

        template<class Context>
        static constexpr auto reshape(Context & context, entt::entity entity, const ui::geometry & geometry) {
            ui::geometry inner_geometry = geometry;

            meta::for_each(contents, [&](auto submodel) {
                using submodel_type = typename plaintype(submodel)::type;

                ui::reshape(context, entity, submodel, inner_geometry);

                if constexpr (nestable<submodel_type>) {
                    auto & sub = context.registry.template get<submodel_type>(entity);
                    sub.nest(context, entity, inner_geometry);
                }
            });

            return geometry;
        }
    };

    template <class ... T>
    constexpr auto nest(T ... models) {
        return meta::type_v<nesting_model<typename T::type...>>;
    }
}
