//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>
#include <ui/systems/geometry_system.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class ... T>
    struct stack_model
    {
        using contents_type = meta::types<T...>;
        static constexpr contents_type contents{};

        template<class Context>
        static constexpr auto reshape(Context & context, entt::entity entity, const ui::geometry & geometry) {
            auto actual_geometry = geometry;

            meta::for_each(contents, [&](auto submodel) {
                const auto g = ui::reshape(context, entity, submodel, geometry);
                actual_geometry.area.include(g.area);
            });

            return actual_geometry;
        }
    };

    template <class ... T>
    constexpr auto stack(T ... models) {
        return meta::type_v<stack_model<typename T::type...>>;
    }
}
