//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>
#include <ui/models/nesting_model.h>

#include <ui/widgets/widget.h>

#include <meta/types/contains.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class T>
    struct toggling_model
    {
        using contents_type = meta::types<T>;
        static constexpr contents_type contents{};

        toggling_model() noexcept = default;
        toggling_model(toggling_model &&) noexcept = default;
        toggling_model & operator = (toggling_model &&) noexcept = default;

        constexpr bool enabled() const {
            return _enabled;
        }

        template <class Context>
        constexpr void nest(Context & context, entt::entity entity, ui::geometry & geometry) const requires ui::nestable<T> {
            if (!_enabled) {
                return;
            }

            context.registry.template get<T>(entity).nest(context, entity, geometry);
        }

        template <class Context>
        constexpr void toggle(Context & context, entt::entity entity) {
            _enabled = !_enabled;

            if (_enabled) {
                ui::populate(context, entity, contents);
                context.registry.template patch<T>(entity);
            } else {
                ui::depopulate(context, entity, contents);
            }
        }

        template<class Context>
        static constexpr ui::geometry reshape(Context & context, entt::entity entity, const ui::geometry & geometry) {
            auto & toggler = context.registry.template get<toggling_model<T>>(entity);

            if (toggler._enabled) {
                return ui::reshape(context, entity, contents, geometry);
            }

            return geometry;
        }

    private:
        bool _enabled = true;
    };

    template <class T>
    constexpr auto toggleable(meta::type<T>) {
        return meta::type_v<toggling_model<T>>;
    }

    namespace detail
    {
        template<class ... Components, class Component>
        constexpr void assert_components(meta::types<Components...>, meta::type<Component>) {
            static_assert(meta::contains_v<meta::types<Components...>, toggling_model<Component>>, "The given component is not declared as toggleable in the provided widget");
        }

        template<class Context, class Widget, class Component, class F>
            requires (std::is_base_of_v<ui::widgets::model_widget, Widget>)
        constexpr void toggle(Context & ctx, Widget & w, meta::type<Component> component_type, F callback) {
            assert_components(ui::collect_components(Widget::model_def), component_type);

            constexpr auto type_key = meta::type_v<toggling_model<Component>>;

            auto & model_ref = w.model_ref();
            model_ref.patch(type_key, [&](auto & toggler) {
                return callback(toggler, model_ref);
            });
        }
    }

    template<class Context, class Widget, class Component>
        requires (std::is_base_of_v<ui::widgets::model_widget, Widget>)
    constexpr void toggle(Context & ctx, Widget & w, meta::type<Component> component_type, bool enabled) {
        ui::detail::toggle(ctx, w, component_type, [&](auto & toggler, auto & model_ref) {
            if (toggler.enabled() == enabled) {
                return false;
            }

            toggler.toggle(ctx, model_ref);
            return true;
        });
    }

    template<class Context, class Widget, class Component>
        requires (std::is_base_of_v<ui::widgets::model_widget, Widget>)
    constexpr void toggle(Context & ctx, Widget & w, meta::type<Component> component_type) {
        ui::detail::toggle(ctx, w, component_type, [&](auto & toggler, auto & model_ref) {
            toggler.toggle(ctx, model_ref);
            return true;
        });
    }
}
