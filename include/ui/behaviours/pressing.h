//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/source.h>
#include <ui/controllers/behaviour_controller.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace behaviours
    {
        template <class T>
        concept pressable_target = requires (T v) {
            { v.set_pressed(true) };
        };

        class pressing
        {
        public:
            struct pressable_tag {};

            template <class T>
            void press(T & target, const space::point &) noexcept {
                if constexpr (pressable_target<T>) {
                    target.set_pressed(true);
                }

                this->_on_pressed_change(&target, true);
            }

            template <class T>
            void release(T & target, const space::point &) noexcept {
                if constexpr (pressable_target<T>) {
                    target.set_pressed(false);
                }

                this->_on_pressed_change(&target, false);
            }

            template <class T, class Destination, class F>
            auto subscribe(Destination & dst, F && callback) {
                return dst.subscribe(_on_pressed_change, [callback{std::forward<F>(callback)}](void * target, bool pressed) {
                    callback(*static_cast<T *>(target), pressed);
                });
            }

        private:
            signal::source<void(void * target, bool pressed)> _on_pressed_change;
        };
    }

    static constexpr struct
    {
        template <class... T> requires (
            symbol::matches<T...> (
                s::on_pressed_change    [symbol::optional][symbol::unique],
                s::hub                  [symbol::optional][symbol::unique]
            )
        )
        auto operator()(T &&... options) const noexcept {
            return [&](auto & target) {
                using target_type = plaintype(target);
                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                ui::behaviours::pressing behaviour;

                if constexpr (symbol::in<T...>(s::on_pressed_change)) {
                    auto & hub = named.at(s::hub, []() -> signal::hub & { return signal::global_hub(); });
                    behaviour.subscribe<target_type>(hub, named[s::on_pressed_change]);
                }

                return behaviour;
            };
        }
    } pressing;
}
