//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <optional>

#include <space/spatial.h>
#include <ui/controllers/behaviour_controller.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace behaviours
    {
        struct scroll_origin
        {
            space::point pos;
            space::point offset;
        };

        class scrolling
        {
        public:
            struct pressable_tag {};
            struct hoverable_tag {};
            struct wheelable_tag {};

            space::real wheel_scroll_amount() const noexcept {
                return _wheel_scroll_amount;
            }

            void set_wheel_scroll_amount(space::real speed) noexcept {
                _wheel_scroll_amount = speed;
            }

            //-----------------------------------------------------------------

            template <class T>
            void press(T & target, const space::point & pos) noexcept {
                _scroll_origin = scroll_origin {pos, target.offset()};
            }

            template <class T>
            void release(T &, const space::point &) noexcept {
                _scroll_origin = {};
            }

            template <class T>
            void hover_enter(T & target, const space::point & pos) noexcept {
                //
            }

            template <class T>
            void hover_leave(T & target, const space::point & pos) noexcept {
                //
            }

            template <class T>
            bool hover_move(T & target, const space::point & pos) noexcept {
                if (!_scroll_origin) {
                    return false;
                }

                return target.set_offset(_scroll_origin->offset + pos - _scroll_origin->pos);
            }

            template <class T>
            void wheel(T & target, const space::point &, const space::point & delta) noexcept {
                target.set_offset(target.offset() + delta * _wheel_scroll_amount);
            }

        protected:
            space::real _wheel_scroll_amount = 8.0f;
            std::optional<scroll_origin> _scroll_origin {};
        };
    }

    static constexpr struct
    {
        template <class... T> requires (
            symbol::matches<T...> (
                s::wheel_scroll_amount [symbol::optional][symbol::unique]
            )
        )
        auto operator()(T &&... options) const noexcept {
            return [&](auto & target) {
                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                ui::behaviours::scrolling behaviour;

                if constexpr (symbol::in<T...>(s::wheel_scroll_amount)) {
                    behaviour.set_wheel_scroll_amount(named[s::wheel_scroll_amount]);
                }

                return behaviour;
            };
        }
    } scrolling;
}
