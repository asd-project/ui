//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <space/spatial.h>
#include <ui/behaviours/scrolling.h>

#include <ui/routines/inertia.h>
#include <ui/routines/context.h>

#include <signal/observer.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace behaviours
    {
        class inertial_scrolling : public scrolling
        {
        public:
            struct touchable_tag {};

            using inertia_type = inertia<math::float_point>;
            using duration_type = typename inertia_type::duration_type;

            template <class T>
            inertial_scrolling(T & target, inertia_type::executor_type & executor) :
                _inertia(executor),
                _scroll_observer(_inertia.on_input, [](void * target, const auto & delta) {
                    auto t = static_cast<T *>(target);
                    return t->set_offset(t->offset() + delta);
                })
            {}

            inertial_scrolling(inertial_scrolling && s) = default;
            inertial_scrolling & operator = (inertial_scrolling && s) = default;

            auto deceleration() const noexcept {
                return _inertia.deceleration();
            }

            void set_deceleration(float deceleration) noexcept {
                _inertia.set_deceleration(deceleration);
            }

            duration_type input_period() const noexcept {
                return _input_period;
            }

            void set_input_period(duration_type period) noexcept {
                _input_period = period;
            }

            template <class T>
            void press(T & target, const space::point & pos) noexcept {
                _inertia.lock();
                this->_scroll_origin = scroll_origin {pos, target.offset()};
            }

            template <class T>
            bool hover_move(T & target, const space::point & pos) noexcept {
                if (!this->_scroll_origin) {
                    return false;
                }

                return _inertia.move(&target, this->_scroll_origin->offset - target.offset() + pos - this->_scroll_origin->pos, _input_period);
            }

            template <class T>
            void release(T & target, const space::point &) noexcept {
                _inertia.unlock(&target);
                this->_scroll_origin = {};
            }

            template <class T>
            void wheel(T & target, const space::point &, const space::point & delta) noexcept {
                _inertia.move(&target, delta * this->_wheel_scroll_amount, _input_period);

                if (!this->_scroll_origin) {
                    _inertia.unlock(&target);
                }
            }

            template <class T>
            void touch_start(T &, const space::point &, int touch_count) noexcept {
                _inertia.lock();
                _touched = true;
            }

            template <class T>
            void touch_end(T &, const space::point &, int touch_count) noexcept {
                if (touch_count == 0) {
                    _touched = false;
                }
            }

            void reset() noexcept {
                _inertia.lock();
            }

        protected:
            inertia_type _inertia;
            duration_type _input_period = 16ms;
            signal::observer<bool(void *, const space::point &)> _scroll_observer;
            bool _touched = false;
        };
    }

    static constexpr struct
    {
        template <class... T> requires (
            symbol::matches<T...> (
                s::routine_context      [symbol::required][symbol::unique],
                s::wheel_scroll_amount  [symbol::optional][symbol::unique],
                s::deceleration         [symbol::optional][symbol::unique],
                s::input_period         [symbol::optional][symbol::unique]
            )
        )
        auto operator()(T &&... options) const noexcept {
            return [&](auto & target) {
                using target_type = plaintype(target);
                using inertia_type = typename ui::behaviours::inertial_scrolling::inertia_type;

                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                ui::behaviours::inertial_scrolling behaviour(target, named[s::routine_context].template executor<inertia_type &>());

                if constexpr (symbol::in<T...>(s::wheel_scroll_amount)) {
                    behaviour.set_wheel_scroll_amount(named[s::wheel_scroll_amount]);
                }

                if constexpr (symbol::in<T...>(s::deceleration)) {
                    behaviour.set_deceleration(named[s::deceleration]);
                }

                if constexpr (symbol::in<T...>(s::input_period)) {
                    behaviour.set_input_period(named[s::input_period]);
                }

                return behaviour;
            };
        }
    } inertial_scrolling;
}
