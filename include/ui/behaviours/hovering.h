//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/source.h>
#include <ui/controllers/behaviour_controller.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace behaviours
    {
        template <class T>
        concept hoverable_target = requires (T v) {
            { v.set_hovered(true) };
        };

        class hovering
        {
        public:
            struct hoverable_tag {};

            template <class T>
            void hover_enter(T & target, const space::point &) noexcept {
                if constexpr (hoverable_target<T>) {
                    target.set_hovered(true);
                }

                this->_on_hovered_change(&target, true);
            }

            template <class T>
            void hover_leave(T & target, const space::point &) noexcept {
                if constexpr (hoverable_target<T>) {
                    target.set_hovered(false);
                }

                this->_on_hovered_change(&target, false);
            }

            template <class T>
            bool hover_move(T & target, const space::point &) noexcept {
                return false;
            }

            template <class T, class Destination, class F>
            auto subscribe(Destination & dst, F && callback) {
                return dst.subscribe(_on_hovered_change, [callback{std::forward<F>(callback)}](void * target, bool hovered) {
                    callback(*static_cast<T *>(target), hovered);
                });
            }

        private:
            signal::source<void(void *, bool)> _on_hovered_change;
        };
    }

    static constexpr struct
    {
        template <class... T> requires (
            symbol::matches<T...> (
                s::on_hovered_change    [symbol::optional][symbol::unique],
                s::hub                  [symbol::optional][symbol::unique]
            )
        )
        auto operator()(T &&... options) const noexcept {
            return [&](auto & target) {
                using target_type = plaintype(target);
                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                ui::behaviours::hovering behaviour;

                if constexpr (symbol::in<T...>(s::on_hovered_change)) {
                    auto & hub = named.at(s::hub, []() -> signal::hub & { return signal::global_hub(); });
                    behaviour.subscribe<target_type>(hub, named[s::on_hovered_change]);
                }

                return behaviour;
            };
        }
    } hovering;
}
