//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/source.h>
#include <ui/controllers/controller.h>

#include <optional>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace behaviours
    {
        class clicking
        {
        public:
            struct pressable_tag {};
            struct movable_tag {};
            struct hoverable_tag {};

            clicking(int move_threshold = 2) :
                _move_threshold(move_threshold) {}

            template <class T>
            void press(T &, const space::point & pos) noexcept {
                _click_pos = pos;
            }

            template <class T>
            void release(T & target, const space::point & pos) noexcept {
                if (!_click_pos) {
                    return;
                }

                if (check_threshold(*_click_pos, pos)) {
                    this->_on_click(&target, *_click_pos);
                }

                _click_pos = {};
            }

            template <class T>
            bool hover_move(T &, const space::point & pos) noexcept {
                if (_click_pos && check_threshold(*_click_pos, pos)) {
                    _click_pos = {};
                }

                return true;
            }

            template <class T>
            void hover_enter(T &, const space::point &) noexcept {
                //
            }

            template <class T>
            void hover_leave(T &, const space::point &) noexcept {
                _click_pos = {};
            }

            template <class T, class Destination, class F>
            auto subscribe(Destination & dst, F && callback) {
                return dst.subscribe(_on_click, [callback{std::forward<F>(callback)}](void * target, const space::point & pos) {
                    callback(*static_cast<T *>(target), pos);
                });
            }


        private:
            bool check_threshold(const space::point & click_pos, const space::point & pos) noexcept {
                return math::square_distance(click_pos, pos) <= math::sqr(_move_threshold);
            }

            int _move_threshold = 2;
            std::optional<space::point> _click_pos;
            signal::source<void(void *, const space::point &)> _on_click;
        };
    }

    static constexpr struct
    {
        template <class... T> requires (
            symbol::matches<T...> (
                s::on_click [symbol::required][symbol::unique],
                s::hub      [symbol::optional][symbol::unique]
            )
        )
        auto operator()(T &&... options) const noexcept {
            return [&](auto & target) {
                using target_type = plaintype(target);
                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                ui::behaviours::clicking behaviour;

                if constexpr (symbol::in<T...>(s::on_click)) {
                    auto & hub = named.at(s::hub, []() -> signal::hub & { return signal::global_hub(); });
                    behaviour.subscribe<target_type>(hub, named[s::on_click]);
                }

                return behaviour;
            };
        }
    } clicking;
}
