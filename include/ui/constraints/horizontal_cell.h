//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/model.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        struct horizontal_cell
        {
            space::range range;

            horizontal_cell(space::range range) noexcept : range(range) {}

            ui::geometry apply(const ui::geometry & geometry) {
                return {{geometry.area.min, space::size{math::clamp(geometry.area.width(), range.min, range.max), geometry.area.height()}}, geometry.depth};
            }
        };
    }
}
