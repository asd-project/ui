//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/model.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        struct constraint
        {
            const ui::geometry & apply(const ui::geometry & geometry) {
                return geometry;
            }

            ui::geometry && apply(ui::geometry && geometry) {
                return std::move(geometry);
            }
        };
    }
}
