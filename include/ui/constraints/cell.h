//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <space/types.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        struct cell
        {
            space::size_range range;

            cell(const space::size_range & range) noexcept : range(range) {}

            ui::geometry apply(const ui::geometry & geometry) {
                return {{geometry.area.min, math::clamp(geometry.area.size(), range.min, range.max)}, geometry.depth};
            }
        };
    }
}
