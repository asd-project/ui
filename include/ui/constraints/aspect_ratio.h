//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <space/types.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        enum class fit
        {
            horizontal,
            vertical,
            cover,
            contain
        };

        template <ui::fit fit>
        struct aspect_ratio
        {
            float ratio = 1.0f;

            aspect_ratio(float ratio = 1.0f) noexcept : ratio(ratio) {}

            ui::geometry apply(const ui::geometry & geometry) {
                if constexpr (fit == ui::fit::horizontal) {
                    return {{geometry.area.min, space::size{geometry.area.width()}}, geometry.depth};
                } else if constexpr (fit == ui::fit::vertical) {
                    return {{geometry.area.min, space::size{geometry.area.height()}}, geometry.depth};
                } else if constexpr (fit == ui::fit::cover) {
                    return {{geometry.area.min, space::size{math::max(geometry.area.width(), geometry.area.height())}}, geometry.depth};
                } else {
                    return {{geometry.area.min, space::size{math::min(geometry.area.width(), geometry.area.height())}}, geometry.depth};
                }
            }
        };
    }
}
