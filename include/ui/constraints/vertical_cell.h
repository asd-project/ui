//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/model.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        struct vertical_cell
        {
            space::range range;

            vertical_cell(space::range range) noexcept : range(range) {}

            ui::geometry apply(const ui::geometry & geometry) {
                return {{geometry.area.min, space::size{geometry.area.width(), math::clamp(geometry.area.height(), range.min, range.max)}}, geometry.depth};
            }
        };
    }
}
