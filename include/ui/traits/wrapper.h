//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/symbols.h>
#include <ui/constraints/constraint.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace traits
        {
            template <class Content, class Constraint = ui::constraint>
            class wrapper
            {
            public:
                wrapper(Content && content, Constraint && constraint = {}) noexcept :
                    _content(std::move(content)),
                    _constraint(std::move(constraint)) {}

                wrapper(wrapper && w) noexcept = default;
                wrapper & operator = (wrapper && w) noexcept = default;

            protected:
                Content _content;
                Constraint _constraint;
            };
        }
    }
}
