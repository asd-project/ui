//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/controllers/bound_controller.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace traits
        {
            template <class ActualType, class ControllerWrapper = std::unique_ptr<bound_controller<ActualType>>>
            class interactive
            {
            public:
                interactive() noexcept {
                    if (_controller) { // controller may be default constructed by wrapper
                        _controller->rebind(static_cast<ActualType *>(this));
                    }
                }

                interactive(ControllerWrapper && controller) noexcept :
                    _controller(std::move(controller))
                {
                    if (_controller) {
                        _controller->rebind(static_cast<ActualType *>(this));
                    }
                }

                interactive(interactive && w) noexcept :
                    _controller(std::move(w._controller))
                {
                    if (_controller) {
                        _controller->rebind(static_cast<ActualType *>(this));
                    }
                }

                interactive & operator = (interactive && w) noexcept {
                    std::swap(_controller, w._controller);

                    if (_controller) {
                        _controller->rebind(static_cast<ActualType *>(this));
                    }

                    if (w._controller) {
                        w._controller->rebind(static_cast<ActualType *>(&w));
                    }

                    return *this;
                }

                void set_controller(ControllerWrapper && c) {
                    _controller = std::move(c);

                    if (_controller) {
                        _controller->rebind(static_cast<ActualType *>(this));
                    }
                }

                void hit(ui::trace & trace, const space::point &) noexcept {
                    trace.push_back(&*_controller);
                }

            protected:
                ControllerWrapper _controller;
            };
        }
    }
}
