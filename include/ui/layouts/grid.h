//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/layouts/layout.h>
#include <ui/widgets/container.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        template<class T>
        concept spanned = requires (T v) {
            { v.span };
        };

        static constexpr struct grow_type {} grow;
        static constexpr struct shrink_type {} shrink;

        static constexpr struct horizontal_type {} horizontal;
        static constexpr struct vertical_type {} vertical;

        namespace layouts
        {
            enum class segment_fit
            {
                none,
                grow,
                shrink
            };

            struct segment
            {
                segment(space::real size) noexcept : size(size) {}
                segment(grow_type) noexcept : fit(segment_fit::grow) {}
                segment(shrink_type) noexcept : fit(segment_fit::shrink) {}

                segment(segment &&) noexcept = default;

                segment & operator = (segment && c) noexcept = default;

                space::real size = 0.0f;
                segment_fit fit = segment_fit::none;
            };

            struct anchor
            {
                space::real offset = 0.0f;
            };

            template <int N>
            struct scheme
            {
                static constexpr int size = N;

                scheme(std::array<layouts::segment, N> && segments) noexcept :
                    segments(std::move(segments))
                {
                    for (auto & s : this->segments) {
                        if (s.fit == segment_fit::none) {
                            static_size += s.size;
                        } else if (s.fit == segment_fit::grow) {
                            ++dynamic_segments_count;
                        }
                    }
                }

                scheme(scheme &&) noexcept = default;

                scheme & operator = (scheme && c) noexcept = default;

                void update(const space::range & range, space::real margin, space::real spacing, int elements_count) {
                    this->range = range;
                    this->margin = margin;
                    this->spacing = spacing;

                    auto dynamic_size = this->dynamic_segments_count > 0 ? (range.size() - this->static_size - margin * 2 - (size - 1) * spacing) / this->dynamic_segments_count : 0.0f;

                    anchors[0].offset = range.min + margin;

                    for (size_t i = 0; i < size; ++i) {
                        auto & s = this->segments[i];
                        this->anchors[i + 1].offset = this->anchors[i].offset + (
                            s.fit == segment_fit::none ?
                                s.size :
                                s.fit == segment_fit::grow ?
                                    dynamic_size :
                                    0.0f
                        ) + spacing;
                    }
                }

                space::range place(int first, int last) {
                    BOOST_ASSERT_MSG(first <= last, "Last index should be greater or equal to first");
                    BOOST_ASSERT_MSG(last + 1 < this->anchors.size(), "Incorrect index");

                    return {this->anchors[first].offset, this->anchors[last + 1].offset - this->spacing};
                }

                void fit(int first, int last, const space::range & element_range) {
                    BOOST_ASSERT_MSG(first <= last, "Last index should be greater or equal to first");
                    BOOST_ASSERT_MSG(last + 1 < this->anchors.size(), "Incorrect index");

                    auto & last_segment = this->segments[last];

                    if (last_segment.fit == segment_fit::none) {
                        return;
                    }

                    if (this->anchors[last + 1].offset < element_range.max + this->spacing) {
                        this->anchors[last + 1].offset = element_range.max + this->spacing;

                        for (int i = last + 1; i < size; ++i) {
                            auto & s = this->segments[i];

                            if (s.fit != segment_fit::none) {
                                break;
                            }

                            this->anchors[i + 1].offset = this->anchors[i].offset + s.size + this->spacing;
                        }
                    }
                }

                space::range actual_range() const {
                    return {anchors.front().offset - margin, anchors.back().offset - spacing + margin};
                }

                std::array<layouts::segment, N> segments;
                std::array<layouts::anchor, N + 1> anchors;
                space::real static_size = 0.0f;
                int dynamic_segments_count = 0;
                space::range range;
                space::real margin = 0.0f;
                space::real spacing = 0.0f;
            };

            struct dynamic_scheme
            {
                static constexpr int size = std::numeric_limits<int>::max();

                dynamic_scheme(layouts::segment && segment) noexcept :
                    segment(std::move(segment))
                {}

                dynamic_scheme(dynamic_scheme &&) noexcept = default;

                dynamic_scheme & operator = (dynamic_scheme && c) noexcept = default;

                void update(const space::range & range, space::real margin, space::real spacing, int elements_count) {
                    this->anchors.clear();
                    this->anchors.push_back({range.min + margin});

                    this->range = range;
                    this->margin = margin;
                    this->spacing = spacing;

                    if (this->segment.fit == segment_fit::grow) {
                        this->segment.size = elements_count > 0 ? (this->range.size() - margin * 2 - (elements_count - 1) * spacing) / elements_count : 0;
                    }
                }

                space::range place(int first, int last) {
                    BOOST_ASSERT_MSG(first <= last, "Last index should be greater or equal to first");

                    for (int i = int(this->anchors.size()); i < last + 2; ++i) {
                        this->anchors.push_back({this->anchors[i - 1].offset + this->segment.size + this->spacing});
                    }

                    return {this->anchors[first].offset, this->anchors[last + 1].offset - this->spacing};
                }

                void fit(int first, int last, const space::range & element_range) {
                    BOOST_ASSERT_MSG(first <= last, "Last index should be greater or equal to first");
                    BOOST_ASSERT_MSG(last + 1 < this->anchors.size(), "Incorrect index");

                    if (this->segment.fit == segment_fit::none) {
                        return;
                    }

                    if (this->anchors[last + 1].offset < element_range.max + this->spacing) {
                        this->anchors[last + 1].offset = element_range.max + this->spacing;

                        for (int i = last + 1; i < int(this->anchors.size() - 1); ++i) {
                            this->anchors[i + 1].offset = this->anchors[i].offset + this->segment.size + this->spacing;
                        }
                    }
                }

                space::range actual_range() const {
                    return {anchors.front().offset - margin, anchors.back().offset - spacing + margin};
                }

                layouts::segment segment;
                space::range range;
                space::real margin = 0.0f;
                space::real spacing = 0.0f;
                std::vector<layouts::anchor> anchors;
            };

            template <class HorizontalScheme, class VerticalScheme, class PrimaryDirection = ui::vertical_type>
            class grid
            {
            public:
                grid(
                    HorizontalScheme && horizontal,
                    VerticalScheme && vertical,
                    space::size spacing,
                    space::size margin
                ) noexcept :
                    _horizontal(std::move(horizontal)),
                    _vertical(std::move(vertical)),
                    _spacing(spacing),
                    _margin(margin)
                {}

                grid(grid &&) noexcept = default;
                grid & operator = (grid && c) noexcept = default;

                template <class Elements>
                ui::geometry update(Elements & elements, const ui::geometry & geometry) noexcept {
                    if constexpr (std::is_same_v<PrimaryDirection, ui::vertical_type>) {
                        _horizontal.update(geometry.area.horizontal, _margin.x, _spacing.x, math::ceil<int>(float(elements.size()) / float(VerticalScheme::size)));
                        _vertical.update(geometry.area.vertical, _margin.y, _spacing.y, math::min<int>(elements.size(), VerticalScheme::size));
                    } else {
                        _horizontal.update(geometry.area.horizontal, _margin.x, _spacing.x, math::min<int>(elements.size(), HorizontalScheme::size));
                        _vertical.update(geometry.area.vertical, _margin.y, _spacing.y, math::ceil<int>(float(elements.size()) / float(HorizontalScheme::size)));
                    }

                    using element_type = plaintype(elements[0]);

                    if constexpr (ui::spanned<element_type>) {
                        for (auto & e : elements) {
                            space::rect area(_horizontal.place(e.span.min.x, e.span.max.x), _vertical.place(e.span.min.y, e.span.max.y));
                            e.update({area, geometry.depth});

                            auto actual_geometry = e.geometry();
                            _horizontal.fit(e.span.min.x, e.span.max.x, actual_geometry.area.horizontal);
                            _vertical.fit(e.span.min.y, e.span.max.y, actual_geometry.area.vertical);
                        }
                    } else {
                        if constexpr (meta::tuple_like<Elements>) {
                            static_assert(std::tuple_size_v<Elements> <= uint64(HorizontalScheme::size) * VerticalScheme::size, "There are too many elements to fit into layout");
                        } else {
                            BOOST_ASSERT_MSG(elements.size() <= uint64(HorizontalScheme::size) * VerticalScheme::size, "There are too many elements to fit into layout");
                        }

                        constexpr auto pos_by_index = [](size_t index) {
                            if constexpr (std::is_same_v<PrimaryDirection, ui::vertical_type>) {
                                return math::uint_point{index / VerticalScheme::size, index % VerticalScheme::size};
                            } else {
                                return math::uint_point{index % HorizontalScheme::size, index / HorizontalScheme::size};
                            }
                        };

                        for (size_t i = 0; i < elements.size(); ++i) {
                            auto & e = elements[i];

                            auto pos = pos_by_index(i);
                            space::rect area(_horizontal.place(pos.x, pos.x), _vertical.place(pos.y, pos.y));
                            e.update({area, geometry.depth});

                            auto actual_geometry = e.geometry();
                            _horizontal.fit(pos.x, pos.x, actual_geometry.area.horizontal);
                            _vertical.fit(pos.y, pos.y, actual_geometry.area.vertical);
                        }
                    }

                    return {space::rect(_horizontal.actual_range(), _vertical.actual_range()), geometry.depth};
                }

                template <class Elements, class ReturnType = decltype(&std::declval<Elements>()[0])>
                ReturnType hit(Elements & elements, const space::point & pos) noexcept {
                    for (auto & e : elements) {
                        if (e.geometry().area.contains(pos)) {
                            return &e;
                        }
                    }

                    return nullptr;
                }

            private:
                HorizontalScheme _horizontal;
                VerticalScheme _vertical;
                space::size _spacing;
                space::size _margin;
            };
        }

        template <class ... T> requires (sizeof ... (T) >= 1)
        layouts::scheme<sizeof...(T)> scheme(T ... values) {
            return std::array{layouts::segment(values) ...};
        }

        template <class T>
        layouts::dynamic_scheme dynamic_scheme(T value) {
            return {layouts::segment(value)};
        }

        static constexpr struct
        {
            template <class... T> requires (
                symbol::matches<T...> (
                    s::spacing      [symbol::required][symbol::unique],
                    s::margin       [symbol::required][symbol::unique],
                    s::vertical     [symbol::optional][symbol::unique],
                    s::horizontal   [symbol::optional][symbol::unique],
                    s::direction    [symbol::optional][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                auto && direction = named.at(s::direction, []() { return ui::vertical; });
                using direction_type = plaintype(direction);

                auto && horizontal = named.at(s::horizontal, []() {
                    if constexpr (std::is_same_v<direction_type, ui::vertical_type>) {
                        return scheme(ui::grow);
                    } else {
                        return dynamic_scheme(ui::shrink);
                    }
                });

                auto && vertical = named.at(s::vertical, []() {
                    if constexpr (std::is_same_v<direction_type, ui::vertical_type>) {
                        return dynamic_scheme(ui::shrink);
                    } else {
                        return scheme(ui::grow);
                    }
                });

                using horizontal_type = plaintype(horizontal);
                using vertical_type = plaintype(vertical);

                using layout_type = ui::layouts::grid<
                    horizontal_type,
                    vertical_type,
                    direction_type
                >;

                return layout_type(
                    std::move(horizontal),
                    std::move(vertical),

                    static_cast<space::size>(named[s::spacing]),
                    static_cast<space::size>(named[s::margin])
                );
            }
        } grid;
    }
}
