//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <space/spatial.h>

#include <ui/widgets/widget.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        template <class T>
        concept layout =
            std::is_nothrow_move_constructible_v<T> &&
            std::is_nothrow_move_assignable_v<T> &&
            requires (T v, const std::vector<ui::widgets::basic_widget> & elements, const ui::geometry & geometry, const space::point & pos) {
                { v.update(elements, geometry) } -> convertible_to<ui::geometry>;
                { v.hit(elements, pos) } -> convertible_to<const ui::widgets::basic_widget *>;
            };
    }
}
