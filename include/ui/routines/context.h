//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/symbols.h>
#include <routine/context.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        using routine_context = routine::context<std::chrono::milliseconds>;
    }
}
