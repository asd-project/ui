//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/function.h>
#include <math/math.h>
#include <color/color.h>
#include <ui/model.h>
#include <routine/executor.h>

#include <chrono>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        class animation : public routine::basic_periodic_task<animation>
        {
        public:
            using base_type = routine::basic_periodic_task<animation>;
            using duration_type = std::chrono::milliseconds;
            using progress_type = float;
            using callback_type = asd::move_only_function<void(progress_type)>;

            animation(base_type::executor_type & executor) noexcept :
                base_type(executor) {}

            animation(animation && a) noexcept = default;
            ~animation() noexcept = default;

            animation & operator = (animation && a) noexcept = default;

            using base_type::active;

            duration_type current_time() const noexcept {
                return _current_time;
            }

            duration_type time_left() const noexcept {
                return _duration - _current_time;
            }

            template <class Duration>
            void set_duration(Duration duration) noexcept {
                auto progress = _duration.count() > 0 ? static_cast<progress_type>(_current_time.count()) / _duration.count() : 0.0f;
                _duration = std::chrono::duration_cast<duration_type>(duration);
                _current_time = std::chrono::duration_cast<duration_type>(_duration * progress);
            }

            template <class Duration>
            void play(Duration duration, callback_type callback) noexcept {
                _current_time = duration_type::zero();
                _duration = std::chrono::duration_cast<duration_type>(duration);
                _callback = std::move(callback);

                resume();
            }

            void stop() noexcept {
                pause();

                _current_time = duration_type::zero();
                _duration = duration_type::zero();
                _callback = {};
            }

            export_api(ui)
            void pause() noexcept;

            export_api(ui)
            void resume() noexcept;

            export_api(ui)
            void set_time(const duration_type & time) noexcept;

            export_api(ui)
            void reset() noexcept;

            export_api(ui)
            void operator()(duration_type delta) noexcept;

            static constexpr auto linear = []<class F>(F && func) noexcept {
                return std::forward<F>(func);
            };

            static constexpr auto ease_in = []<class F>(F && func) noexcept {
                return [func{std::forward<F>(func)}](progress_type progress) mutable {
                    func(progress * progress);
                };
            };

            static constexpr auto ease_out = []<class F>(F && func) noexcept {
                return [func{std::forward<F>(func)}](progress_type progress) mutable {
                    func(math::fast::sqrt(progress));
                };
            };

            static constexpr auto ease_in_out = []<class F>(F && func) noexcept {
                return [func{std::forward<F>(func)}](progress_type progress) mutable {
                    func(0.5f * (1.0f + math::fast::sin((progress - 0.5f) * math::pi)));
                };
            };

        private:
            void update() noexcept;

            duration_type _current_time = duration_type::zero();
            duration_type _duration = duration_type::zero();
            callback_type _callback;
        };

        template <auto T>
        class model_animation;

        template <class PropertyType, class Component, PropertyType Component::* Property>
        class model_animation<Property> : public animation
        {
        public:
            using component_type = Component;
            using property_type = PropertyType;

            using animation::animation;
            using animation::operator=;

            template<class Duration, class Easing>
            void animate(const ecs::ref & model_ref, const property_type & to, const Duration & duration, const Easing & easing) {
                constexpr auto type_key = meta::type_v<component_type>;

                auto & property = model_ref[type_key].*Property;

                if (property == to) {
                    this->pause();
                    return;
                }

                this->play(duration, easing([model_ref, from = property, to, type_key](progress_type t) {
                    model_ref.patch(type_key, [&](auto & component) {
                        if constexpr (std::is_same_v<plaintype(from), color::linear_rgb>) {
                            component.*Property = color::hue_lerp(from, to, t);
                        } else {
                            component.*Property = math::lerp(from, to, t);
                        }
                    });
                }));
            }
        };
    }
}
