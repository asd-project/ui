//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/common.h>
#include <meta/unique_value.h>

#include <signal/member_source.h>

#include <routine/executor.h>

#include <chrono>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        using namespace std::chrono_literals;

        template <class Delta>
        class inertia : public routine::basic_periodic_task<inertia<Delta>>
        {
        public:
            using base_type = routine::basic_periodic_task<inertia<Delta>>;
            using delta_type = Delta;
            using duration_type = std::chrono::duration<float>;

            inertia(typename base_type::executor_type & executor) noexcept :
                base_type(executor)
            {}

            inertia(inertia && a) noexcept = default;
            ~inertia() noexcept = default;

            inertia & operator = (inertia && a) noexcept = default;

            using base_type::active;

            float deceleration() const noexcept {
                return _deceleration;
            }

            void set_deceleration(float deceleration) noexcept {
                _deceleration = deceleration;
            }

            bool move(void * target, const delta_type & delta, duration_type duration) {
                auto active = false;
                on_input([&](bool m) { active |= m; }, target, delta);

                this->set_active(active);

                if (active) {
                    _current_speed = _current_speed + delta / duration.count();
                } else {
                    _current_speed = {};
                }

                return active;
            }

            void lock() {
                _target = nullptr;
                _current_speed = {};
            }

            void unlock(void * target) {
                if (_target == target) {
                    return;
                }

                _target = target;
                this->set_active(true);
            }

            void operator()(duration_type dt) {
                if (!_target) {
                    _current_speed = {};
                    this->set_active(false);
                    return;
                }

                auto seconds = dt.count();
                auto speed_delta = _deceleration * seconds;

                const auto move_to_zero = [&](auto & value) {
                    value = value < 0 ? value + math::min(-value, speed_delta) : value - math::min(value, speed_delta);
                };

                const auto speed_magnitude = [&] {
                    if constexpr (math::is_point_like_v<delta_type>) {
                        _current_speed.apply(move_to_zero);
                        return math::square_length(_current_speed);
                    } else {
                        move_to_zero(_current_speed);
                        return math::sqr(_current_speed);
                    }
                }();

                if (speed_magnitude < math::eps) {
                    this->set_active(false);
                    lock();

                    return;
                }

                bool active = false;
                on_input([&](bool m) { active |= m; }, _target.value, _current_speed * seconds);

                this->set_active(active);

                if (!active) {
                    lock();
                }
            }

            signal::member_source<inertia, bool(void *, const delta_type &)> on_input;

        private:
            // settings
            float _deceleration = 1000.0f;

            // state
            delta_type _current_speed;
            meta::unique_value<void *> _target;
        };
    }
}
