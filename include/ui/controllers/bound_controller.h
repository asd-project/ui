//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/controllers/controller.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class T>
    class bound_controller : public controller
    {
    public:
        virtual void rebind(T * target) {
            _target = target;
        }

    protected:
        T * _target = nullptr;
    };

}
