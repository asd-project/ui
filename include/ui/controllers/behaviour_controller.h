//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/controllers/bound_controller.h>

#include <meta/tuple/accumulate.h>
#include <meta/tuple/for_each.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class Behaviour>
    concept pressable_behaviour = requires { typename Behaviour::pressable_tag; };

    template <class Behaviour>
    concept hoverable_behaviour = requires { typename Behaviour::hoverable_tag; };

    template <class Behaviour>
    concept wheelable_behaviour = requires { typename Behaviour::wheelable_tag; };

    template <class Behaviour>
    concept touchable_behaviour = requires { typename Behaviour::touchable_tag; };

    template <class Behaviour>
    concept resetable_behaviour = requires (Behaviour b) { { b.reset() }; };

    namespace controllers
    {
        template <class T, class ... Behaviours>
        class behaviour_controller : public bound_controller<T>
        {
        public:
            behaviour_controller() = default;
            behaviour_controller(Behaviours && ... behaviours) requires (sizeof ... (Behaviours) > 0) :
                _behaviours(std::move(behaviours)...) {}

            void press(const space::point & pos) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (pressable_behaviour<plaintype(behaviour)>) {
                        behaviour.press(*this->_target, pos);
                    }
                });
            }

            void release(const space::point & pos) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (pressable_behaviour<plaintype(behaviour)>) {
                        behaviour.release(*this->_target, pos);
                    }
                });
            }

            void hover_enter(const space::point & pos) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (hoverable_behaviour<plaintype(behaviour)>) {
                        behaviour.hover_enter(*this->_target, pos);
                    }
                });
            }
            void hover_leave(const space::point & pos) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (hoverable_behaviour<plaintype(behaviour)>) {
                        behaviour.hover_leave(*this->_target, pos);
                    }
                });
            }

            bool hover_move(const space::point & pos) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                return meta::accumulate(_behaviours, false, [&](bool acc, auto & behaviour) {
                    if constexpr (hoverable_behaviour<plaintype(behaviour)>) {
                        auto moving = behaviour.hover_move(*this->_target, pos);
                        return acc || moving;
                    }

                    return acc;
                });
            }

            void wheel(const space::point & pos, const space::point & delta) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (wheelable_behaviour<plaintype(behaviour)>) {
                        behaviour.wheel(*this->_target, pos, delta);
                    }
                });
            }

            void touch_start(const space::point & pos, int touch_count) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (touchable_behaviour<plaintype(behaviour)>) {
                        behaviour.touch_start(*this->_target, pos, touch_count);
                    }
                });
            }

            void touch_end(const space::point & pos, int touch_count) noexcept override final {
                BOOST_ASSERT_MSG(this->_target, "Target should not be null");

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (touchable_behaviour<plaintype(behaviour)>) {
                        behaviour.touch_end(*this->_target, pos, touch_count);
                    }
                });
            }

            void rebind(T * target) override final {
                bound_controller<T>::rebind(target);

                meta::for_each(_behaviours, [&](auto & behaviour) {
                    if constexpr (resetable_behaviour<plaintype(behaviour)>) {
                        behaviour.reset();
                    }
                });
            }

            template <class Behaviour>
            bool has() const {
                constexpr int index = meta::find_v<std::tuple<Behaviours...>, Behaviour>;
                return index >= 0;
            }

            template <class Behaviour>
            auto & behaviour() {
                constexpr int index = meta::find_v<std::tuple<Behaviours...>, Behaviour>;
                static_assert(index >= 0, "Controller doesn't contain given behaviour");

                return get<index>(_behaviours);
            }

            template <class Behaviour>
            auto & operator[](meta::type<Behaviour>) {
                return this->behaviour<Behaviour>();
            }

        protected:
            std::tuple<Behaviours...> _behaviours;
        };
    }

    static constexpr struct
    {
        auto operator()(auto &&... behaviours) const noexcept {
            return [&](auto & area) {
                using controller_type = ui::controllers::behaviour_controller<plaintype(area), plaintype(behaviours(area))...>;
                return std::make_unique<controller_type>(behaviours(area)...);
            };
        }
    } behaviour_controller;
}
