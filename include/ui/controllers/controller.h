//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <vector>

#include <boost/type_traits.hpp>

#include <space/spatial.h>

#include <ui/symbols.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    class controller
    {
    public:
        virtual ~controller() {}

        virtual void press(const space::point & pos) noexcept {}
        virtual void release(const space::point & pos) noexcept {}

        virtual void hover_enter(const space::point & pos) noexcept {}
        virtual void hover_leave(const space::point & pos) noexcept {}
        virtual bool hover_move(const space::point & pos) noexcept {
            return false;
        }

        virtual void wheel(const space::point & pos, const space::point & delta) noexcept {}

        virtual void touch_start(const space::point & pos, int touch_count) noexcept {}
        virtual void touch_end(const space::point & pos, int touch_count) noexcept {}
    };

    using trace = std::vector<controller *>;

    template <class T>
    concept hittable = requires (T v, ui::trace & trace, const space::point & pos) {
        { v.hit(trace, pos) };
    };
}
