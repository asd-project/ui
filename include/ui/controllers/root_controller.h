//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/controllers/controller.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace controllers
    {
        template <class T>
        class root_controller : public ui::controller
        {
        public:
            root_controller(T & target) : _target(target) {}

            void press(const space::point & pos) noexcept override final {
                _pressed_trace = _current_trace;

                for (auto c : _pressed_trace) {
                    c->press(pos);
                }
            }

            bool hover_move(const space::point & pos) noexcept override final {
                check_hit(pos);

                for (auto c : _current_trace) {
                    if (c->hover_move(pos)) {
                        return true;
                    }
                }

                return !_current_trace.empty();
            }

            void release(const space::point & pos) noexcept override final {
                for (auto c : _pressed_trace) {
                    c->release(pos);
                }

                _pressed_trace.clear();
            }

            void wheel(const space::point & pos, const space::point & delta) noexcept override final {
                for (auto c : _current_trace) {
                    c->wheel(pos, delta);
                }

                check_hit(pos);
            }

            void touch_start(const space::point & pos, int touch_count) noexcept override final {
                for (auto c : _current_trace) {
                    c->touch_start(pos, touch_count);
                }
            }

            void touch_end(const space::point & pos, int touch_count) noexcept override final {
                for (auto c : _current_trace) {
                    c->touch_end(pos, touch_count);
                }
            }

        private:
            void check_hit(const space::point & pos) {
                std::swap(_previous_trace, _current_trace);
                _current_trace.clear();

                _target.hit(_current_trace, pos);

                if (_previous_trace == _current_trace) {
                    return;
                }

                for (auto c : _previous_trace) {
                    if (std::find(_current_trace.begin(), _current_trace.end(), c) == _current_trace.end()) {
                        c->hover_leave(pos);
                    }
                }

                for (auto c : _current_trace) {
                    if (std::find(_previous_trace.begin(), _previous_trace.end(), c) == _previous_trace.end()) {
                        c->hover_enter(pos);
                    }
                }
            }

            T & _target;
            ui::trace _previous_trace;
            ui::trace _current_trace;
            ui::trace _pressed_trace;
        };
    }
}
