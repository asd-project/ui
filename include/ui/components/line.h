//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <geometry/line_mesh.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class LineMesh>
    struct line
    {
        using mesh_type = LineMesh;

        mesh_type mesh;
        color::linear_rgb color;
    };
}
