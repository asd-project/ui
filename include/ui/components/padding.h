//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/models/nesting_model.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class Tag>
    struct padding
    {
        void nest(entt::registry &, entt::entity, ui::geometry & geometry) const noexcept {
            geometry.area.resize_by(-width);
        }

        space::real width = 0.0f;
    };

    template <class Tag>
    struct multi_padding
    {
        void nest(entt::registry &, entt::entity, ui::geometry & geometry) const noexcept {
            geometry.area.resize_by(-offsets[0], -offsets[1], -offsets[2], -offsets[3]);
        }

        std::array<space::real, 4> offsets;
    };

    static_assert(nestable<padding<meta::empty_t>>);
    static_assert(nestable<multi_padding<meta::empty_t>>);
}
