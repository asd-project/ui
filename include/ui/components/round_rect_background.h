//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>

//---------------------------------------------------------------------------

namespace asd::ui::components
{
    struct round_rect_background
    {
    };

    namespace def
    {
        constexpr asd::meta::type<asd::ui::components::round_rect_background> round_rect_background {};
    }
}
