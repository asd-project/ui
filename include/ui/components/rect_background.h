//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>

//---------------------------------------------------------------------------

namespace asd::ui::components
{
    struct rect_background
    {
    };

    namespace def
    {
        constexpr asd::meta::type<asd::ui::components::rect_background> rect_background {};
    }
}
