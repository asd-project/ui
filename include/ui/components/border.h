//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>

//---------------------------------------------------------------------------

namespace asd::ui::components
{
    struct border
    {
    };

    namespace def
    {
        constexpr asd::meta::type<asd::ui::components::border> border {};
    }
}
