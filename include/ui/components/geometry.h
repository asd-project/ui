//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <space/types.h>
#include <compare>

//---------------------------------------------------------------------------

namespace asd::ui
{
    struct geometry
    {
        friend constexpr std::partial_ordering operator <=> (const geometry &, const geometry &) = default;

        friend constexpr geometry operator + (const geometry & g, auto v) {
            return {g.area + v, g.depth};
        }

        friend constexpr geometry operator - (const geometry & g, auto v) {
            return {g.area - v, g.depth};
        }

        math::float_rect area;
        space::real depth = 0.0f;
    };

    namespace def
    {
        constexpr meta::type<ui::geometry> geometry {};
    }
}
