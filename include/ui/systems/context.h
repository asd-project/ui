//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/model.h>
#include <ui/environment.h>

#include <signal/hub.h>

#include <meta/tuple/contains_all_of.h>
#include <meta/tuple/transform.h>

#include <ecs/context.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace tags
    {
        using namespace ecs::tags;

        constexpr auto stateful = meta::type_v<struct stateful_tag>;
        constexpr auto render = meta::type_v<struct render_tag>;
    }

    /**
     * Bound systems declare specific components they are associated with
     */
    template <class T>
    concept bound_system = ecs::system<T> && requires {
        { T::components };
    };

    /**
     * Stateful systems can be populated based on associated components
     * They are required to have two methods:
     *      populate(Context &, entt::entity, ModelDef)
     *      depopulate(Context &, entt::entity, ModelDef)
     */
    template <class T>
    concept stateful_system = ui::bound_system<T> && ecs::has_tag<T>(tags::stateful);

    /**
     * Render systems are required to have render(Context &|entt::registry &) method
     */
    template <class T>
    concept render_system = ecs::system<T> && ecs::has_tag<T>(tags::render);

//---------------------------------------------------------------------------

    template <class SystemsTypes, class ModelDef>
    void populate(ecs::context<SystemsTypes> & context, entt::entity entity, ModelDef) {
        constexpr auto component_defs = ui::collect_components(ModelDef{});

        meta::accumulate(context.systems, component_defs, [&context, entity](auto component_defs, auto & system) {
            using system_type = plaintype(system);
            using component_types = plaintype(component_defs);

            if constexpr (ui::stateful_system<system_type> && meta::contains_all_of(component_types{}, system_type::components)) {
                system.populate(context, entity, ModelDef{});
            }

            return component_defs;
        });
    }

    template <class SystemsTypes, class ModelDef>
    void depopulate(ecs::context<SystemsTypes> & context, entt::entity entity, ModelDef) {
        constexpr auto component_defs = ui::collect_components(ModelDef{});

        meta::accumulate(context.systems, component_defs, [&context, entity](auto component_defs, auto & system) {
            using system_type = plaintype(system);
            using component_types = plaintype(component_defs);

            if constexpr (ui::stateful_system<system_type> && meta::contains_all_of(component_types{}, system_type::components)) {
                system.depopulate(context, entity, ModelDef{});
            }

            return component_defs;
        });
    }

    template <class SystemsTypes>
    void render(ecs::context<SystemsTypes> & ctx) {
        meta::for_each(ctx.systems, [&](auto & system) {
            if constexpr (ui::render_system<plaintype(system)>) {
                system.render(ctx);
            }
        });
    }

    template <class SystemsTypes, class ModelDef>
    ecs::handle make_model(ecs::context<SystemsTypes> & ctx, ModelDef model_def) {
        ecs::handle model_handle{ctx.registry};

        constexpr auto component_defs = ui::collect_components(model_def);
        meta::for_each(component_defs, [&](auto def) { model_handle.emplace(def); });

        ui::populate(ctx, model_handle, model_def);

        return model_handle;
    }
}
