//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/uniform.h>
#include <gfx/shader.h>
#include <gfx/mesh.h>
#include <gfx/render_pass.h>
#include <gfx/pipeline.h>

#include <ui/components/line.h>
#include <ui/systems/context.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    using namespace std::string_view_literals;

    constexpr auto map_primitives_type(geom::primitives_type type) {
        switch (type) {
            case geom::primitives_type::triangles:
                return gfx::primitives_type::triangles;
            case geom::primitives_type::lines:
                return gfx::primitives_type::lines;
            default:
                throw std::runtime_error("Unsupported primitives type");
        }
    }

    template <class Graphics, class MeshType>
    class line_render_system
    {
    public:
        using graphics_type = Graphics;
        using model_type = ui::line<MeshType>;

        using mesh_type = MeshType;
        using point_type = typename mesh_type::point_type;
        using point_model_type = typename point_type::model_type;

        static constexpr auto dimensions = point_model_type::dimensions;

        using vertex_layout = std::conditional_t<dimensions == 3, gfx::vertex_layouts::p3t3, gfx::vertex_layouts::p2t3>;
        static_assert(vertex_layout::instance.size_in_bytes() == sizeof(float) * (dimensions + 3));

        static constexpr auto components = meta::type_v<model_type>;
        static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

        static constexpr std::string_view shader_name = mesh_type::primitives_type == geom::primitives_type::lines ?
            (dimensions == 3 ? "line3d_frame"sv : "line2d_frame"sv) :
            (dimensions == 3 ? "line3d"sv : "line2d"sv);

        static constexpr auto & shader_code = ui::shaders[shader_name];

        static constexpr int priority = mesh_type::primitives_type == geom::primitives_type::lines ? 100 : 0;

        struct render_data
        {
            gfx::handle<gfx::mesh<graphics_type>> mesh;
            gfx::vertex_buffer<graphics_type> * vertex_buffer;
            gfx::uniform<graphics_type, color::linear_rgb> color;
        };

        line_render_system(
            entt::registry & registry,
            gfx::mesh_registry<graphics_type> & meshes,
            gfx::uniform_registry<graphics_type> & uniforms,
            gfx::shader_registry<graphics_type> & shaders,
            gfx::pipeline_registry<graphics_type> & pipelines
        ) :
            _meshes(meshes),
            _color_uniform(uniforms.factory(gfx::uniforms::Color)),
            _shader(shaders.create({shader_name, shader_code})),
            _pipeline(pipelines.create({*_shader})),
            _observer(registry, entt::collector.update<model_type>().template where<render_data>())
        {}

        line_render_system(const line_render_system &) = delete;
        line_render_system(line_render_system &&) = delete;

        template <class ModelDef>
        void populate(entt::registry & registry, entt::entity entity, ModelDef) {
            auto & m = registry.get<model_type>(entity);

            auto mesh = _meshes.create({vertex_layout::instance, map_primitives_type(mesh_type::primitives_type)});
            auto & vb = mesh->add_vertex_buffer(vertex_layout::instance, m.mesh.vertices());
            mesh->set_indices(m.mesh.indices());

            registry.emplace<render_data>(entity, std::move(mesh), &vb, _color_uniform.create(m.color));
        }

        template <class ModelDef>
        void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.erase<render_data>(entity);
        }

        auto reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
            return geometry;
        }

        void update(entt::registry & registry) {
            _observer.each([&](const auto entity) {
                auto [m, r] = registry.get<model_type, render_data>(entity);

                r.mesh->set_indices(m.mesh.indices());
                r.vertex_buffer->update(m.mesh.vertices());
                r.color = m.color;
            });
        }

        void render(entt::registry & registry) {
            _pipeline->activate();

            auto view = registry.view<render_data>();

            view.each([this](auto entity, auto & r) {
                if (r.vertex_buffer->vertices_count > 0) {
                    r.color.activate();
                    r.mesh->render();
                }
            });
        }

    private:
        gfx::mesh_registry<graphics_type> & _meshes;
        gfx::uniform_factory<graphics_type, color::linear_rgb> _color_uniform;
        gfx::handle<gfx::shader_program<graphics_type>> _shader;
        gfx::handle<gfx::pipeline<graphics_type>> _pipeline;
        entt::observer _observer;
    };

    template <class Graphics, class MeshType>
    constexpr meta::type<line_render_system<Graphics, MeshType>> line_render_system_type {};
}
