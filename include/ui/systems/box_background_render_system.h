//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>

//---------------------------------------------------------------------------

namespace asd::ui::systems
{
    template <class T>
    class box_background_render_system
    {
    public:
        using source_type = meta::empty_t;  // TODO: Set source type

        struct destination_type
        {
            // TODO: fill destination data
        };

        static constexpr auto components = meta::type_v<source_type>;
        static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

        box_background_render_system(entt::registry & registry) :
            _observer(registry, entt::collector.update<source_type>().where<destination_type>())
        {}

        box_background_render_system(const box_background_render_system &) = delete;
        box_background_render_system(box_background_render_system &&) = delete;

        template <class Context, class ModelDef>
        void populate(Context & context, entt::entity entity, ModelDef) {
            // TODO: init destination data
            context.registry.emplace<destination_type>(entity);
        }

        template <class ModelDef>
        void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.erase<destination_type>(entity);
        }

        void update(entt::registry & registry) {
            _observer.each([&](auto entity) {
                auto [s, d] = registry.get<source_type, destination_type>(entity);

                // TODO: process updated data
            });
        }

    private:
        entt::observer _observer;
    };
}
