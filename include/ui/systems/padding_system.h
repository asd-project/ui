//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/components/padding.h>
#include <ui/systems/geometry_system.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    template <class Tag>
    class padding_system : public ui::nested_geometry_system<padding<Tag>>
    {
    public:
        using ui::nested_geometry_system<padding<Tag>>::nested_geometry_system;
    };
}
