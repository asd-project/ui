//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>

//---------------------------------------------------------------------------

namespace ui::systems
{
    template <class T>
    class basic_render_system
    {
    public:
        using source_type = meta::empty_t;

        struct destination_type
        {
            // TODO: fill destination data
        };

        static constexpr auto components = meta::type_v<source_type>;

        basic_render_system(entt::registry & registry) :
            _observer(registry, entt::collector.update<source_type>().where<destination_type>())
        {}

        basic_render_system(const basic_render_system &) = delete;
        basic_render_system(basic_render_system &&) = delete;

        template <class Context, class ModelDef>
        void populate(Context & context, entt::entity entity, ModelDef) {
            // TODO: init destination data
            context.registry.emplace<destination_type>(entity);
        }

        template <class ModelDef>
        void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.erase<destination_type>(entity);
        }

        void update(entt::registry & registry) {
            _observer.each([&](auto entity) {
                auto [s, d] = registry.get<source_type, destination_type>(entity);

                // TODO: process updated data
            });
        }

    private:
        entt::observer _observer;
    };
}
