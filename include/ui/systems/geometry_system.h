//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>
#include <ui/components/geometry.h>

#include <spdlog/spdlog.h>

//---------------------------------------------------------------------------

namespace asd::ui
{
    namespace tags
    {
        constexpr auto reshaping = meta::type_v<struct reshaping_tag>;
    }

    template <class T>
    concept reshaping_system = ui::bound_system<T> && ecs::has_tag<T>(ui::tags::reshaping);

    template<class Context, class ModelDef>
    ui::geometry reshape(Context & context, entt::entity entity, ModelDef model_def, const ui::geometry & geometry) {
        using model_type = typename plaintype(model_def)::type;

        auto actual_geometry = geometry;

        meta::for_each(context.systems, [&](auto & system) {
            using system_type = plaintype(system);

            if constexpr (ui::reshaping_system<system_type>) {
                if constexpr (meta::get<0>(system_type::components) == ModelDef{}) {
                    const auto g = system.reshape(context, entity, actual_geometry);
                    actual_geometry.area.include(g.area);
                }
            }
        });

        if constexpr (container_model<model_type>) {
            const auto g = model_type::reshape(context, entity, actual_geometry);
            actual_geometry.area.include(g.area);
        }

        return actual_geometry;
    }

    class geometry_system
    {
    public:
        struct updater
        {
            using callback_type = void(*)(ecs::basic_context &, entt::entity, int);
            callback_type callback;
        };

        static constexpr auto components = ui::def::geometry;
        static constexpr auto tags = ui::tags::stateful + ui::tags::active;

        geometry_system(entt::registry & registry) :
            _observer(registry, entt::collector.update<ui::geometry>().where<updater>())
        {}

        geometry_system(const geometry_system &) = delete;
        geometry_system(geometry_system &&) = delete;

        template <class Context, class ModelDef>
        void populate(Context & context, entt::entity entity, ModelDef) {
            constexpr auto update_callback = +[](ecs::basic_context & context, entt::entity entity, int reshaping_retries) {
                constexpr ModelDef model_def{};

                auto & ctx = static_cast<Context &>(context);
                auto geometry = ctx.registry.template get<ui::geometry>(entity);

                for (auto i = 0; i < reshaping_retries; ++i) {
                    auto actual_geometry = ui::reshape(ctx, entity, model_def, geometry);

                    if (actual_geometry.area == geometry.area) {
                        return;
                    }

                    geometry = actual_geometry;
                }

                spdlog::warn("Reshaping failed after {} retries", reshaping_retries);
            };

            context.registry.template emplace<updater>(entity, update_callback);
        }

        template <class ModelDef>
        void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.erase<updater>(entity);
        }

        void update(ecs::basic_context & context) {
            _observer.each([&](auto entity) {
                context.registry.template get<updater>(entity).callback(context, entity, _reshaping_retries);
            });
        }

    private:
        entt::observer _observer;
        int _reshaping_retries = 5;
    };

    template <class Component>
    class nested_geometry_system
    {
    public:
        static constexpr auto components = meta::type_v<Component>;
        static constexpr auto tags = meta::empty;

        nested_geometry_system(entt::registry & registry) {
            registry.on_update<Component>().template connect<&nested_geometry_system::update_geometry>(this);
        }

        void update_geometry(entt::registry & registry, entt::entity entity) {
            registry.patch<ui::geometry>(entity);
        }
    };

    namespace def
    {
        constexpr meta::type<ui::geometry_system> geometry_system{};
    }
}
