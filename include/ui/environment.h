//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/mesh.h>
#include <gfx/multimesh.h>
#include <gfx/uniform.h>
#include <gfx/shader.h>
#include <gfx/pipeline.h>
#include <gfx/render_pass.h>
#include <gfx/render_target.h>

#include <gfx/font_source.h>
#include <gfx/raster_font.h>
#include <gfx/text_shape.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        namespace vertex_attributes
        {
            struct g : gfx::vertex_attributes::attribute<float, 4>
            {
                static constexpr char key[] = "g";
                static constexpr char name[] = "geometry";
            };

            struct s : gfx::vertex_attributes::attribute<float, 4>
            {
                static constexpr char key[] = "s";
                static constexpr char name[] = "shadow";
            };
        }

        namespace vertex_layouts
        {
            using p3c4g = generator<gfx::vertex_attributes::p3, gfx::vertex_attributes::c4, gfx::vertex_attributes::g>;
            using p3t2c4gs = generator<gfx::vertex_attributes::p3, gfx::vertex_attributes::t2, gfx::vertex_attributes::c4, gfx::vertex_attributes::g, gfx::vertex_attributes::s>;
            using c4g = generator<gfx::vertex_attributes::c4, gfx::vertex_attributes::g>;
            using c4gs = generator<gfx::vertex_attributes::c4, gfx::vertex_attributes::g, gfx::vertex_attributes::s>;
        }
    }
}

#include <ui/shaders.h>

namespace asd
{
    namespace ui
    {
        template <class Gfx, class FontContext = meta::empty_t>
        struct default_environment
        {
            static_assert(gfx::graphics<Gfx>);

            using graphics_type = Gfx;
            using font_context_type = FontContext;

            gfx::mesh_registry<Gfx> & meshes;
            gfx::multimesh_registry<Gfx> & multimeshes;
            gfx::uniform_registry<Gfx> & uniforms;
            gfx::shader_registry<Gfx> & shaders;
            gfx::pipeline_registry<Gfx> & pipelines;
            gfx::render_pass_registry<Gfx> & passes;
            gfx::render_target_registry<Gfx> & render_targets;

            gfx::font_source<FontContext> & font_source;
            gfx::text_shaper<FontContext> & text_shaper;
            gfx::font_rasterizer<FontContext, Gfx> & font_rasterizer;
        };
    }
}
