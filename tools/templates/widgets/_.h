//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>

//---------------------------------------------------------------------------

namespace ${NAMESPACE}
{
    class ${FILE_NAME_LOWER} : public ui::widgets::model_widget
    {
    public:
        using component_type = meta::empty_t;

        struct def : widgets::model_widget::def
        {
            static constexpr meta::type<component_type> component{};
        };

        static constexpr auto model_def = ui::nest(
            def::geometry,
            def::component
        );

        ${FILE_NAME_LOWER}(ecs::handle && model) noexcept :
            model_widget(std::move(model))
        {
            _model.patch(def::component, [this](auto & component) {
                // TODO: init components
            });
        }

        ${FILE_NAME_LOWER}(${FILE_NAME_LOWER} &&) noexcept = default;
        ${FILE_NAME_LOWER} & operator = (${FILE_NAME_LOWER} &&) noexcept = default;
    };
}
