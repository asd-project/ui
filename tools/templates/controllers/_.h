//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/controllers/bound_controller.h>

//---------------------------------------------------------------------------

namespace ${NAMESPACE}
{
    template <class T>
    class ${FILE_NAME_LOWER} : public ui::bound_controller<T>
    {
    public:

    };
}
