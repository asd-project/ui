//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>

//---------------------------------------------------------------------------

namespace ${NAMESPACE}
{
    template <class T>
    class ${FILE_NAME_LOWER}
    {
    public:
        using source_type = meta::empty_t;  // TODO: Set source type

        struct destination_type
        {
            // TODO: fill destination data
        };

        static constexpr auto components = meta::type_v<source_type>;
        static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

        ${FILE_NAME_LOWER}(entt::registry & registry) :
            _observer(registry, entt::collector.update<source_type>().where<destination_type>())
        {}

        ${FILE_NAME_LOWER}(const ${FILE_NAME_LOWER} &) = delete;
        ${FILE_NAME_LOWER}(${FILE_NAME_LOWER} &&) = delete;

        template <class Context, class ModelDef>
        void populate(Context & context, entt::entity entity, ModelDef) {
            // TODO: init destination data
            context.registry.emplace<destination_type>(entity);
        }

        template <class ModelDef>
        void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
            registry.erase<destination_type>(entity);
        }

        void update(entt::registry & registry) {
            _observer.each([&](auto entity) {
                auto [s, d] = registry.get<source_type, destination_type>(entity);

                // TODO: process updated data
            });
        }

    private:
        entt::observer _observer;
    };
}
