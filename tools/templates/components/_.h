//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>

//---------------------------------------------------------------------------

namespace ${NAMESPACE}
{
    struct ${FILE_NAME_LOWER}
    {
    };

    namespace def
    {
        constexpr asd::meta::type<${NAMESPACE}::${FILE_NAME_LOWER}> ${FILE_NAME_LOWER} {};
    }
}
