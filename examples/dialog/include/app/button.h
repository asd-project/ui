//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>

#include <ui/routines/animation.h>
#include <ui/routines/context.h>

#include <ui/widgets/widget.h>
#include <ui/traits/interactive.h>

#include <ui/models/nesting_model.h>
#include <ui/models/stacking_model.h>
#include <ui/models/toggling_model.h>

#include <signal/source.h>

//---------------------------------------------------------------------------

namespace asd::app
{
    using namespace color::literals;
    using namespace std::chrono_literals;

    namespace s
    {
        using namespace ui::s;
    }

    struct padding_layer
    {
        constexpr void nest(const ecs::ref & ref, ui::geometry & g) const {
            g.area.resize_by(-offset, -offset);
        }

        space::real offset = 0.0f;
    };

    struct color_layer
    {
        template <class Context, class Model>
        struct renderer;

        color::linear_rgb color;
    };

    struct border_layer
    {
        template <class Context, class Model>
        struct renderer;

        constexpr void nest(const ecs::ref & ref, ui::geometry & g) const {
            g.area.resize_by(-width, -width);
        }

        color::linear_rgb color;
        space::real width = 0.0f;
    };

    struct shadow_layer
    {
        template <class Context, class Model>
        struct renderer;

        color::linear_rgb color;
        space::point offset;
        space::real radius = 0.0f;
        space::real blur = 0.0f;
    };

    namespace widgets
    {
        class button :
            public ui::widgets::model_widget,
            public ui::traits::interactive<button>
        {
        public:
            struct def : ui::widgets::model_widget::def
            {
                static constexpr meta::type<border_layer> border{};
                static constexpr meta::type<shadow_layer> shadow{};
                static constexpr meta::type<color_layer> background{};
                static constexpr meta::type<padding_layer> padding{};
            };

            static constexpr auto model_def = ui::nest(
                ui::toggleable(def::padding),
                ui::stack(
                    def::shadow,
                    ui::nest(ui::toggleable(def::border), def::background)
                )
            );

            button(ecs::handle && model, routine::executor<ui::animation &> & animations) noexcept  :
                ui::widgets::model_widget(std::move(model)),
                _padding_animation(animations),
                _background_animation(animations),
                _shadow_animation(animations)
            {
                auto [background, border, shadow] = _model(def::background, def::border, def::shadow);

                background.color = _color;

                border.color = 0xfff_rgb;
                border.width = 2.0f;
                border.radius = 2.0f;

                shadow.color = 0x7000_rgb;
                shadow.offset = {0.0f, 0.0f};
                shadow.radius = 2.5f;
                shadow.blur = 1.5f;
            }

            button(button && btn) noexcept = default;

            button & operator = (button && btn) noexcept = default;

            const color::linear_rgb & color() const noexcept {
                return _color;
            }

            void set_color(const color::linear_rgb & color) noexcept {
                if (_color == color) {
                    return;
                }

                _color = color;
                _model.patch(def::background, [&](auto & b) { b.color = _color; });

                update_animations();
            }

            void set_hovered(bool hovered) noexcept {
                if (_hovered == hovered) {
                    return;
                }

                _hovered = hovered;
                update_animations();
            }

            void set_pressed(bool pressed) noexcept {
                if (_pressed == pressed) {
                    return;
                }

                _pressed = pressed;
                update_animations();
            }

        private:
            void update_animations() noexcept {
                _padding_animation.animate(_model,
                    _pressed ?
                        4.0f : 0.0f,

                    _pressed ?
                        0.075s : 0.2s,

                    ui::animation::ease_in_out
                );

                _background_animation.animate(_model,
                    _pressed ?
                        color::darken(_color, 0.1f) :
                        _hovered ?
                            color::lighten(_color, 0.1f) :
                            _color,

                    _pressed || _hovered ?
                        0.1s : 0.2s,

                    ui::animation::ease_in_out
                );
            }

            bool _pressed = false;
            bool _hovered = false;
            color::linear_rgb _color = 0xd24329_rgb;

            ui::model_animation<&padding_layer::offset> _padding_animation;
            ui::model_animation<&color_layer::color> _background_animation;
            ui::model_animation<&shadow_layer::offset> _shadow_animation;
        };
    }

    struct button : app::widgets::button::def
    {
        template <class... T> requires (
            symbol::matches<T...> (
                s::context          [symbol::required][symbol::unique],
                s::routine_context  [symbol::required][symbol::unique],
                s::color            [symbol::optional][symbol::unique],
                s::controller       [symbol::optional][symbol::unique]
            )
        )
        static auto create(T &&... options) noexcept {
            auto [named, _] = symbol::collect(std::forward<T>(options)...);

            auto & context = named[s::context];

            widgets::button btn(ui::make_model(context, widgets::button::model_def), named[s::routine_context].executor<ui::animation &>());

            const auto obtain_controller = [&] {
                if constexpr (symbol::in<T...>(s::controller)) {
                    if constexpr (std::invocable<symbol::named_type<s::type::controller, T...>, widgets::button &>) {
                        return named[s::controller](btn);
                    } else {
                        return named[s::controller];
                    }
                } else {
                    return std::make_unique<ui::bound_controller<widgets::button>>();
                }
            };

            btn.set_controller(obtain_controller());

            if constexpr (symbol::in<T...>(s::color)) {
                btn.set_color(named[s::color]);
            }

            return btn;
        }
    };
}
