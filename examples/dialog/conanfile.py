import os

from conans import ConanFile, CMake

project_name = "dialog"


class DialogConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    topics = ("asd", project_name)
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.launch/0.0.1@asd/testing",
        "asd.ui/0.0.1@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", src="include", dst="include")
        self.copy("*dialog*", src="bin", dst="bin", keep_path=False)
