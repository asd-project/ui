import os

from conans import ConanFile, CMake, tools


class PanelTestConan(ConanFile):
    name = "asd.panel_test"
    version = "0.0.1"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.launch/0.0.1@asd/testing",
        "asd.scene/0.0.1@asd/testing",
        "asd.sdlpp/0.0.1@asd/testing",
        "asd.gfx3d_templates/0.0.1@asd/testing",
        "asd.ui/0.0.1@asd/testing",
        "di/[>=1.2]"
    )
    default_options = {
        "di:with_extensions": True,
        "di:diagnostics_level": 2
    }

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy('*.so*', dst='bin', src='lib')

    def test(self):
        if not tools.cross_building(self.settings):
            os.chdir("bin")
            self.run(".%spanel_test" % os.sep)
