//---------------------------------------------------------------------------

#include <app/widgets/panel.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        namespace widgets
        {
            using namespace std::chrono_literals;

            void panel::set_hovered(bool hovered) noexcept {
                if (_hovered == hovered) {
                    return;
                }

                _hovered = hovered;

                update_padding();
                update_color();
            }

            void panel::set_pressed(bool pressed) noexcept {
                if (_pressed == pressed) {
                    return;
                }

                _pressed = pressed;

                update_padding();
                update_color();
            }

            void panel::set_color(const color::linear_rgb & color) noexcept {
                if (_color == color) {
                    return;
                }

                _color = color;
                _background_animation.stop();

                _model.patch(def::background, [&](auto & b) {
                    b.color = _pressed ?
                        color::darken(_color, 0.1f) :
                        _hovered ?
                            color::lighten(_color, 0.1f) :
                            _color;
                });
            }

            void panel::update_padding() noexcept {
                _padding_animation.animate(_model,
                    _pressed ?
                        4.0f : 0.0f,

                    _pressed ?
                        0.075s : 0.2s,

                    ui::animation::ease_in_out
                );
            }

            void panel::update_color() noexcept {
                _background_animation.animate(_model,
                    _pressed ?
                        color::darken(_color, 0.1f) :
                        _hovered ?
                            color::lighten(_color, 0.1f) :
                            _color,

                    _pressed || _hovered ?
                        0.1s : 0.2s,

                    ui::animation::ease_in_out
                );
            }
        }
    }
}

//---------------------------------------------------------------------------
