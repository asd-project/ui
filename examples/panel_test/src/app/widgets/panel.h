//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/components/geometry.h>
#include <ui/components/padding.h>

#include <opengl/opengl.h>
#include <opengl/uniform.h>
#include <opengl/shader.h>
#include <opengl/mesh.h>
#include <opengl/render_target.h>
#include <opengl/render_pass.h>
#include <opengl/pipeline.h>

#include <ui/routines/animation.h>
#include <ui/routines/context.h>

#include <ui/widgets/widget.h>
#include <ui/traits/interactive.h>

#include <ui/models/nesting_model.h>
#include <ui/models/stacking_model.h>
#include <ui/models/toggling_model.h>

#include <symbol/symbol.h>

#include <signal/source.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        using namespace color::literals;

        using outer_padding = ui::padding<struct outer_tag>;
        using border_padding = ui::padding<struct border_tag>;

        struct border_layer
        {
            color::linear_rgb color;
        };

        struct rect_background
        {
            color::linear_rgb color;
        };

        struct shadow_layer
        {
            color::linear_rgb color;
            space::point offset;
            space::real radius = 0.0f;
            space::real blur = 0.0f;
        };

        namespace widgets
        {
            class panel :
                public widgets::model_widget,
                public traits::interactive<panel>
            {
            public:
                struct def : widgets::model_widget::def
                {
                    static constexpr meta::type<ui::rect_background> background{};
                    static constexpr meta::type<ui::border_layer> border{};
                    static constexpr meta::type<ui::shadow_layer> shadow{};

                    static constexpr meta::type<ui::outer_padding> outer_padding{};
                    static constexpr meta::type<ui::border_padding> border_padding{};
                };

                static constexpr auto model_def = ui::nest(
                    def::geometry,
                    ui::toggleable(def::outer_padding),
                    ui::stack(
                        def::shadow,
                        ui::nest(
                            def::border,
                            ui::toggleable(def::border_padding),
                            def::background
                        )
                    )
                );

                panel(ecs::handle && model, ui::animation::executor_type & animations) noexcept :
                    model_widget(std::move(model)),
                    _padding_animation(animations),
                    _background_animation(animations),
                    _shadow_animation(animations)
                {
                    _model.patch(def::background + def::border + def::border_padding + def::shadow, [this](auto & background, auto & border, auto & border_padding, auto & shadow) {
                        background.color = _color;

                        border.color = 0xfff_rgb;

                        border_padding.width = 2.0f;

                        shadow.color = 0x7000_rgb;
                        shadow.offset = {0.0f, 0.0f};
                        shadow.radius = 2.5f;
                        shadow.blur = 1.5f;
                    });
                }

                panel(panel && p) noexcept = default;

                panel & operator = (panel && p) noexcept = default;

                const color::linear_rgb & color() const noexcept {
                    return _color;
                }

                export_api(ui)
                void set_color(const color::linear_rgb & color) noexcept;

                export_api(ui)
                void set_hovered(bool hover) noexcept;

                export_api(ui)
                void set_pressed(bool pressed) noexcept;

            private:
                void update_color() noexcept;
                void update_padding() noexcept;

                bool _pressed = false;
                bool _hovered = false;
                color::linear_rgb _color = 0xd24329_rgb;

                ui::model_animation<&outer_padding::width> _padding_animation;
                ui::model_animation<&rect_background::color> _background_animation;
                ui::model_animation<&shadow_layer::offset> _shadow_animation;
            };
        }

        struct panel : ui::widgets::panel::def
        {
            template <class... T> requires (
                symbol::matches<T...> (
                    s::context          [symbol::required][symbol::unique],
                    s::routine_context  [symbol::required][symbol::unique],
                    s::color            [symbol::optional][symbol::unique],
                    s::controller       [symbol::optional][symbol::unique]
                )
            )
            static auto create(T &&... options) noexcept {
                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                widgets::panel p(ui::make_model(named[s::context], widgets::panel::model_def), named[s::routine_context].template executor<ui::animation &>());

                auto controller = [&] {
                    if constexpr (symbol::in<T...>(s::controller)) {
                        if constexpr (std::invocable<symbol::named_type<s::type::controller, T...>, widgets::panel &>) {
                            return named[s::controller](p);
                        } else {
                            return named[s::controller];
                        }
                    } else {
                        return std::make_unique<ui::bound_controller<widgets::panel>>();
                    }
                }();

                p.set_controller(std::move(controller));

                if constexpr (symbol::in<T...>(s::color)) {
                    p.set_color(named[s::color]);
                }

                return p;
            }
        };
    }
}
