//---------------------------------------------------------------------------

#include <launch/main.h>
#include <sdlpp/module.h>

#include <scene/scene.h>

#include <flow/run_main.h>
#include <flow/timer.h>

#include <math/fmt.h>

#include <app/widgets/panel.h>
#include <app/systems/panel_render_systems.h>

#include <uio/shortcut.h>

#include <ui/systems/context.h>
#include <ui/systems/geometry_system.h>

#include <ui/widgets/box.h>
#include <ui/widgets/container.h>
#include <ui/widgets/scroll_area.h>

#include <ui/layouts/grid.h>

#include <ui/behaviours/hovering.h>
#include <ui/behaviours/pressing.h>
#include <ui/behaviours/clicking.h>
#include <ui/behaviours/inertial_scrolling.h>

#include <ui/controllers/root_controller.h>
#include <ui/constraints/aspect_ratio.h>

#include <boost/di/extension/scopes/scoped.hpp>

//---------------------------------------------------------------------------

namespace app
{
    using namespace asd;
    using namespace std::chrono_literals;
    using namespace color::literals;

    constexpr color::linear_rgb clear_color = 0xfff_rgb;

    using ui_graphics = opengl::graphics;

    constexpr auto systems =
        ui::def::geometry_system +
        ui::def::panel_systems<ui_graphics>;

    struct gui
    {
        using allocator_type = pmr::polymorphic_allocator<gui>;

        using context_type = ecs::context_of<systems>;
        using environment_type = ui::default_environment<ui_graphics>;

        gui(
            context_type & context,
            ui::routine_context & routines,
            environment_type & environment,
            allocator_type alloc
        ) :
            context(context),
            routines(routines),
            camera_view(environment.uniforms),
            render_pass(environment.passes.create({{}, gfx::blending::interpolative, clear_color})),
            hub{alloc}
        {
            this->camera.set_projection(scene::projection::screen);
            this->camera.set_far_distance(2.0f);
            this->camera.set_position({0.0f, 0.0f, -1.0f});
            this->camera.set_direction({0.0f, 0.0f, 1.0f});
        }

        void update() {
            this->camera_view.update(this->camera);
            ecs::update(this->context);
        }

        void render() {
            this->render_pass->start();
            this->camera_view.activate();
            ui::render(this->context);
            this->render_pass->end();
        }

        context_type & context;
        ui::routine_context & routines;

        scene::camera camera;
        scene::camera_view<ui_graphics> camera_view;
        gfx::handle<gfx::render_pass<ui_graphics>> render_pass;

        signal::hub hub;
    };

    auto create_panel(app::gui & gui, const color::linear_rgb & c) {
        using namespace ui::s;
        using ui::s::color;

        return ui::panel::create(
            context = gui.context,
            routine_context = gui.routines,
            color = c,
            controller = ui::behaviour_controller(
                ui::hovering(),
                ui::pressing(),
                ui::clicking(
                    hub = gui.hub,
                    on_click = [&](auto & panel, const auto &) {
                        ui::toggle(gui.context, panel, ui::panel::outer_padding);
                        ui::toggle(gui.context, panel, ui::panel::border_padding);
                    }
                )
            )
        );
    }

    auto create_view(app::gui & gui) {
        using namespace ui::s;
        constexpr auto base_color = "#d24329"_rgb;

        return ui::scroll_area(
            constraint = ui::aspect_ratio<ui::fit::horizontal>{},

            controller = ui::behaviour_controller(
                ui::inertial_scrolling(
                    routine_context = gui.routines,
                    wheel_scroll_amount = 8.0f,
                    deceleration = 1600.0f,
                    input_period = 12ms
                )
            ),

            ui::container::make_static(
                layout = ui::grid(
                    horizontal = ui::scheme(ui::grow, ui::grow, ui::grow),
                    vertical = ui::scheme(ui::grow, ui::grow, ui::grow),
                    spacing = 8.0f,
                    margin = 8.0f
                ),

                ui::div(
                    ui::span<0, 1>, ui::span<0>,
                    create_panel(gui, color::rotate(base_color, 0.0f))
                ),
                ui::div(
                    ui::span<2>, ui::span<0, 1>,
                    create_panel(gui, color::rotate(base_color, 0.1f))
                ),
                ui::div(
                    ui::span<1, 2>, ui::span<2>,
                    create_panel(gui, color::rotate(base_color, 0.2f))
                ),
                ui::div(
                    ui::span<0>, ui::span<1, 2>,
                    create_panel(gui, color::rotate(base_color, 0.4f))
                ),
                ui::div(
                    ui::span<1>, ui::span<1>,
                    create_panel(gui, color::rotate(base_color, 0.5f))
                )
            )
        );
    }

    struct app_container
    {
        using allocator_type = pmr::polymorphic_allocator<app_container>;

        sdl::app & app;
        sdl::graphics & graphics;
        app::gui & ui;
        sdl::window & window;
        sdl::mouse & mouse;
        sdl::keyboard & keyboard;

        allocator_type alloc;

        boost::asio::io_context io_context;

        std::unique_ptr<SDL_Cursor, void(*)(SDL_Cursor *)> arrow_cursor{SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW), SDL_FreeCursor};
        std::unique_ptr<SDL_Cursor, void(*)(SDL_Cursor *)> hand_cursor{SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND), SDL_FreeCursor};

        ui::widgets::box root{create_view(ui)};
        ui::controllers::root_controller<ui::widgets::box> controller{root};

        uio::shortcut_context shortcuts{keyboard, ui.hub};

        bool fullscreen = false;

        app_container(
            sdl::app & app,
            sdl::graphics & graphics,
            app::gui & ui,
            sdl::window & window,
            sdl::mouse & mouse,
            sdl::keyboard & keyboard,
            allocator_type alloc
        ) :
            app(app),
            graphics(graphics),
            ui(ui),
            window(window),
            mouse(mouse),
            keyboard(keyboard),
            alloc(alloc)
        {}

        app_container(const app_container &) = delete;
        app_container & operator = (const app_container &) = delete;

        int run() {
            using namespace uio::literals;

            this->shortcuts.subscribe("Alt+Enter"_shortcut, [this](const uio::shortcut &) {
                this->fullscreen = !this->fullscreen;

                if (this->fullscreen) {
                    SDL_SetWindowFullscreen(this->window, SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL);
                } else {
                    SDL_SetWindowFullscreen(this->window, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
                }
            });

            this->shortcuts.subscribe("Ctrl+Q"_shortcut | "Cmd+Q"_shortcut, [this](const uio::shortcut &) {
                this->app.quit();
            });

            this->ui.hub.subscribe(this->mouse.on_move, [this](const uio::mouse_move_event & event) {
                if (this->controller.hover_move(event.mouse.position())) {
                    SDL_SetCursor(this->hand_cursor.get());
                } else {
                    SDL_SetCursor(this->arrow_cursor.get());
                }
            });

            this->ui.hub.subscribe(this->mouse.on_press, [this](const uio::mouse_button_event & event) {
                if (event.button != uio::mouse_button::left) {
                    return;
                }

                this->controller.press(event.mouse.position());
            });

            this->ui.hub.subscribe(this->mouse.on_release, [this](const uio::mouse_button_event & event) {
                if (event.button != uio::mouse_button::left) {
                    return;
                }

                this->controller.release(event.mouse.position());
            });

            this->ui.hub.subscribe(this->mouse.on_wheel, [this](const uio::mouse_wheel_event & event) {
                this->controller.wheel(event.mouse.position(), event.delta);
            });

            this->ui.hub.subscribe(this->window.on_resize, [&](const auto & logical_size, const auto & physical_size) {
                spdlog::info("{}", physical_size);

                this->graphics.set_viewport(physical_size);
                this->ui.camera.set_viewport(logical_size);
            });

            auto update_timer = flow::periodic_timer<std::chrono::milliseconds>::start(this->io_context, 16ms, [&](auto elapsed) {
                this->ui.routines(elapsed);

                this->root.update({this->window.logical_size()});

                this->ui.update();
                this->ui.render();

                this->graphics.present();
            });

            auto event_timer = flow::timer::start(this->io_context, 16ms, [&]() {
                if (!this->app.process_events()) {
                    this->io_context.stop();
                }
            });

            this->graphics.set_viewport(this->window.physical_size());
            this->ui.camera.set_viewport(this->window.logical_size());

            flow::run_main(io_context);

            return 0;
        }
    };

    template <class T>
    auto bind_memory_resource(pmr::memory_resource * resource) {
        using allocator_type = typename T::allocator_type;
        return boost::di::bind<allocator_type>().to(allocator_type{resource});
    }

    inline auto configure_default_memory_resource()
    {
        auto default_resource = pmr::new_delete_resource();
        pmr::set_default_resource(default_resource);

        return default_resource;
    }

    pmr::monotonic_buffer_resource common_memory_resource_upstream{configure_default_memory_resource()};
    pmr::unsynchronized_pool_resource common_memory_resource{&common_memory_resource_upstream};

    auto window_module() {
        namespace di = boost::di;

#if ASD_OPENGL_ES
        opengl::configuration opengl_config{opengl::profile::es, 3, 0, 0}; // opengl es
#else
        opengl::configuration opengl_config{opengl::profile::core, 3, 3, 0}; // desktop opengl
#endif

        return di::make_injector(
            di::bind<opengl::configuration>().to(std::move(opengl_config)),
            di::bind<sdl::window_title>().to("ui::panel"),
            di::bind<sdl::window_size>().to(sdl::window_size{640, 640}),

            bind_memory_resource<uio::mouse_input>(&common_memory_resource),
            bind_memory_resource<uio::keyboard_input>(&common_memory_resource),
            bind_memory_resource<ui::routine_context>(&common_memory_resource),
            bind_memory_resource<sdl::app>(&common_memory_resource) [di::override],
            bind_memory_resource<app::gui>(&common_memory_resource),
            bind_memory_resource<app::app_container>(&common_memory_resource)
        );
    }

    static entrance open([] {
        try {
            auto root_module = boost::di::make_injector(
                sdl::module(),
                window_module()
            );

            auto & c = root_module.create<app_container &>();

            return c.run();
        } catch (const std::exception & e) {
            spdlog::error(e.what());
            return -1;
        }
    });
}

//---------------------------------------------------------------------------
