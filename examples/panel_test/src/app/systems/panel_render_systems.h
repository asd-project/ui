//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <app/widgets/panel.h>

#include <gfx3d_templates/mesh_quad.h>

#include <gfx/uniform.h>
#include <gfx/shader.h>
#include <gfx/mesh.h>
#include <gfx/multimesh.h>
#include <gfx/render_pass.h>
#include <gfx/pipeline.h>

#include <color/fmt.h>

#include <opengl/opengl.h>

#include <spdlog/spdlog.h>

#include <ui/systems/padding_system.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace ui
    {
        using outer_padding_system = ui::padding_system<struct outer_tag>;
        using border_padding_system = ui::padding_system<struct border_tag>;

        template <class Graphics>
        class rect_background_render_system
        {
        public:
            using graphics_type = Graphics;
            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p3>;

            struct render_data
            {
                gfx::multimesh<graphics_type, color::linear_rgb, space::rect> mesh;
            };

            static constexpr auto components = ui::panel::def::background;
            static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

            static constexpr auto & shader_code = ui::shaders["quad"];

            rect_background_render_system(
                entt::registry & registry,
                gfx::multimesh_registry<graphics_type> & multimeshes,
                gfx::shader_registry<graphics_type> & shaders,
                gfx::pipeline_registry<graphics_type> & pipelines
            ) :
                _mesh_scheme(quads::create<gfx::vertex_layouts::c4g>(multimeshes)),
                _shader(shaders.create({"quad", shader_code})),
                _pipeline(pipelines.create({*_shader})),
                _observer(registry, entt::collector.update<ui::rect_background>().where<render_data>())
            {}

            rect_background_render_system(const rect_background_render_system &) = delete;
            rect_background_render_system(rect_background_render_system &&) = delete;

            template <class ModelDef>
            void populate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.emplace<render_data>(entity, _mesh_scheme->template instance<color::linear_rgb, space::rect>());
            }

            template <class ModelDef>
            void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.erase<render_data>(entity);
            }

            auto reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
                auto & r = registry.get<render_data>(entity);
                r.mesh.template assign<1>(geometry.area);

                return geometry;
            }

            void update(entt::registry & registry) {
                _observer.each([&](const auto entity) {
                    auto [m, r] = registry.get<ui::rect_background, render_data>(entity);
                    r.mesh.template assign<0>(m.color);
                });
            }

            void render(entt::registry &) {
                _pipeline->activate();
                _mesh_scheme->render();
            }

        private:
            gfx::handle<gfx::multimesh_scheme<graphics_type>> _mesh_scheme;
            gfx::handle<gfx::shader_program<graphics_type>> _shader;
            gfx::handle<gfx::pipeline<graphics_type>> _pipeline;
            entt::observer _observer;
        };

        template <class Graphics>
        class rect_border_render_system
        {
        public:
            using graphics_type = Graphics;
            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p3>;

            struct render_data
            {
                gfx::multimesh<graphics_type, color::linear_rgb, space::rect> mesh;
            };

            static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;
            static constexpr auto components = ui::panel::def::border + ui::panel::def::background;

            static constexpr auto & shader_code = ui::shaders["quad"];

            rect_border_render_system(
                entt::registry & registry,
                gfx::multimesh_registry<graphics_type> & multimeshes,
                gfx::shader_registry<graphics_type> & shaders,
                gfx::pipeline_registry<graphics_type> & pipelines
            ) :
                _mesh_scheme(quads::create<gfx::vertex_layouts::c4g>(multimeshes, [](size_t index, auto & pos) {
                    return make_plain_tuple(std::array{pos[0], pos[1], pos[2] + 0.1f});
                })),
                _shader(shaders.create({"quad", shader_code})),
                _pipeline(pipelines.create({*_shader})),
                _observer(registry, entt::collector.update<border_layer>().where<render_data>())
            {}

            rect_border_render_system(const rect_border_render_system &) = delete;
            rect_border_render_system(rect_border_render_system &&) = delete;

            template <class ModelDef>
            void populate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.emplace<render_data>(entity, _mesh_scheme->template instance<color::linear_rgb, space::rect>());
            }

            template <class ModelDef>
            void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.erase<render_data>(entity);
            }

            auto reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
                auto & r = registry.get<render_data>(entity);
                r.mesh.template assign<1>(geometry.area);

                return geometry;
            }

            void update(entt::registry & registry) {
                _observer.each([&](const auto entity) {
                    auto [m, r] = registry.get<border_layer, render_data>(entity);
                    r.mesh.template assign<0>(m.color);
                });
            }

            template <class Context>
            void render(Context &) {
                _pipeline->activate();
                _mesh_scheme->render();
            }

        private:
            gfx::handle<gfx::multimesh_scheme<graphics_type>> _mesh_scheme;
            gfx::handle<gfx::shader_program<graphics_type>> _shader;
            gfx::handle<gfx::pipeline<graphics_type>> _pipeline;
            entt::observer _observer;
        };

        template <class Graphics>
        class shadow_render_system
        {
        public:
            using graphics_type = Graphics;
            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p3t2>;

            struct render_data
            {
                gfx::multimesh<graphics_type, color::linear_rgb, space::rect, space::vector> mesh;
            };

            static constexpr auto components = ui::panel::def::shadow;
            static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

            static constexpr auto & shader_code = ui::shaders["shadow"];

            shadow_render_system(
                entt::registry & registry,
                gfx::multimesh_registry<graphics_type> & multimeshes,
                gfx::shader_registry<graphics_type> & shaders,
                gfx::pipeline_registry<graphics_type> & pipelines
            ) :
                _mesh_scheme(quads::create<gfx::vertex_layouts::c4gs>(multimeshes, [](size_t index, auto & pos, auto & t) {
                    return make_plain_tuple(std::array{pos[0], pos[1], pos[2] + 0.2f}, t);
                })),
                _shader(shaders.create({"shadow", shader_code})),
                _pipeline(pipelines.create({*_shader})),
                _observer(registry, entt::collector.update<shadow_layer>().where<render_data>())
            {}

            shadow_render_system(const shadow_render_system &) = delete;
            shadow_render_system(shadow_render_system &&) = delete;

            template <class ModelDef>
            void populate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.emplace<render_data>(entity, _mesh_scheme->template instance<color::linear_rgb, space::rect, space::vector>());
            }

            template <class ModelDef>
            void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.erase<render_data>(entity);
            }

            auto reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
                auto [m, r] = registry.get<shadow_layer, render_data>(entity);

                auto shadow_area = geometry.area.resized_by(m.radius);
                auto blur = 0.5f * m.blur / shadow_area.size();
                auto radius = m.radius / shadow_area.size();

                r.mesh.template assign<1, 2>(shadow_area + m.offset, space::vector(blur.x, blur.y, radius.x, radius.y));

                return geometry;
            }

            void update(entt::registry & registry) {
                _observer.each([&](const auto entity) {
                    auto [m, r] = registry.get<shadow_layer, render_data>(entity);
                    r.mesh.template assign<0>(m.color);
                });
            }

            template <class Context>
            void render(Context &) {
                _pipeline->activate();
                _mesh_scheme->render();
            }

        private:
            gfx::handle<gfx::multimesh_scheme<graphics_type>> _mesh_scheme;
            gfx::handle<gfx::shader_program<graphics_type>> _shader;
            gfx::handle<gfx::pipeline<graphics_type>> _pipeline;
            entt::observer _observer;
        };

        namespace def
        {
            constexpr meta::type<ui::outer_padding_system> outer_padding_system{};

            constexpr meta::type<ui::border_padding_system> border_padding_system{};

            template <class Graphics>
            constexpr meta::type<ui::rect_background_render_system<Graphics>> rect_background_render_system{};

            template <class Graphics>
            constexpr meta::type<ui::rect_border_render_system<Graphics>> rect_border_render_system{};

            template <class Graphics>
            constexpr meta::type<ui::shadow_render_system<Graphics>> shadow_render_system{};

            template <class Graphics>
            constexpr auto panel_systems =
                def::outer_padding_system +
                def::border_padding_system +
                def::shadow_render_system<Graphics> +
                def::rect_border_render_system<Graphics> +
                def::rect_background_render_system<Graphics>;
        }
    }
}
