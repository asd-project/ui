#--------------------------------------------------------
#    widget test
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

#--------------------------------------------------------

project(panel_test VERSION 0.0.1)

#--------------------------------------------------------

include(asd/bootstrap)
include(asd/module)

#--------------------------------------------------------

module(EXAMPLE)
    domain(app)
    group(src Headers)
    files(
        gui_context.h
        gui_view.h

        components/
            border.h
            color.h
            shadow.h
            ..
        systems/
            border.h
            color.h
            shadow.h
            panel_render_systems.h
            ..
        widgets/
            panel.h
            switch_view.h
            flow_frame.h
            tiling_frame.h
            ..
    )

    group(src Sources)
    files(
        main.cpp
        widgets/
            panel.cpp
            ..
    )

    emscripten_shell_file("emscripten/template.html")
endmodule()

#--------------------------------------------------------
